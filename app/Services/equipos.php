<?php

namespace App\Services;

use App\Laboratorio;

class equipos
{
    public function get()
    {
        $laboratorio = Laboratorio::get();
        $laboratorioArray[''] = 'Selecciona un laboratorio';
        foreach ($laboratorios as $laboratorio) {
            $laboratorioArray[$laboratorio->id] = $laboratorio->nombre;
        }
        return $laboratorioArray;
    }
}
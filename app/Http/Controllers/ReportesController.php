<?php

namespace App\Http\Controllers;
use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\ReportesExport;

class ReportesController extends Controller
{
    function showReporteIncidencias(){
    	return $this->printData('reporte_incidencias');
    }

    function showReporteIncidenciasFecha(){
    	return $this->printData('reporte_incidencias_fecha');
    }

    public function equiposPorMaquina(Request $request){    	
    	$request->validate([
    		'_token' => "required|string",
    		'laboratorio' => "required"
    	]);
    	$equipos = App\Computadora::where('laboratorio_id', $request->laboratorio)->get();
    	return $equipos;
    }

    public function getData(Request $request){
    	//dd($request->all());
    	if (isset($request->type)) {
    		$request->validate([
    			'laboratorio' => "required",
    			'equipo' => "required"
    		]);
    		$datos = DB::table('uso_incidentes')->select('uso_incidentes.id', 
						'laboratorios.nombre as laboratorio', 
						DB::raw("concat(computadoras.numero,' ',computadoras.marca,' ',computadoras.modelo) as equipo"), 
						DB::raw("concat(users.nombre,' ',users.apellido_paterno,' ',users.apellido_materno) as usuario"), 
						'tipos_de_incidentes.nombre as incidente',
						'uso_incidentes.detalles as comentario',
                        'prioridades.nombre as prioridad', 
						'status.nombre as status',
						'uso_incidentes.created_at as fecha')
    					->join('usos', 'uso_incidentes.id_uso', '=', 'usos.id_uso')
    					->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
    					->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
    					->join('users', 'usos.id_usuario', '=', 'users.id')
    					->join('tipos_de_incidentes', 'uso_incidentes.id_TipoIncidente', '=', 'tipos_de_incidentes.id')
    					->join('status', 'uso_incidentes.status', '=', 'status.id')
                        ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
    					->where([
    						['usos.id_laboratiorio', $request->laboratorio],
    						['usos.id_computadora', $request->equipo]
    					])->get();
    		/*dd($datos);*/
    		return $this->printData('reporte_incidencias', $datos)->withErrors([
                'laboratorioOk' => $request->laboratorio,
                'equipoOk' => $request->equipo
            ]);
    	}else{
    		$request->validate([
    			'laboratorio' => "required",
    			'equipo' => "required",
    			'fecha' => "required"
    		]);
    		$datos = DB::table('uso_incidentes')->select('uso_incidentes.id', 
						'laboratorios.nombre as laboratorio', 
						DB::raw("concat(computadoras.numero,' ',computadoras.marca,' ',computadoras.modelo) as equipo"), 
						DB::raw("concat(users.nombre,' ',users.apellido_paterno,' ',users.apellido_materno) as usuario"), 
						'tipos_de_incidentes.nombre as incidente',
						'uso_incidentes.detalles as comentario', 
                        'prioridades.nombre as prioridad', 
						'status.nombre as status',
						'uso_incidentes.created_at as fecha')
    					->join('usos', 'uso_incidentes.id_uso', '=', 'usos.id_uso')
    					->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
    					->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
    					->join('users', 'usos.id_usuario', '=', 'users.id')
    					->join('tipos_de_incidentes', 'uso_incidentes.id_TipoIncidente', '=', 'tipos_de_incidentes.id')
    					->join('status', 'uso_incidentes.status', '=', 'status.id')
                        ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
    					->where([
    						['usos.id_laboratiorio', $request->laboratorio],
    						['usos.id_computadora', $request->equipo],
    						[DB::raw('date(uso_incidentes.created_at)'), $request->fecha]
    					])->get();
    		/*dd($datos);*/
    		return $this->printData('reporte_incidencias_fecha', $datos)->withErrors([
                'laboratorioOk' => $request->laboratorio,
                'equipoOk' => $request->equipo,
                'fechaOk' => $request->fecha
            ]);
    	}
    }

    public function printData($view='',$datos=[]){
        $view = 'reportes.'.$view;
        $laboratorios = App\Laboratorio::all();
        return view($view, compact('laboratorios', 'datos'));
    }

    public function export($laboratorio, $equipo, $tipo='xlsx'){
        return (new ReportesExport($laboratorio, $equipo))->download('reportes_laboratorios.'.$tipo);
    }

    public function exportF($laboratorio, $equipo, $fecha, $tipo='xlsx'){
        return (new ReportesExport($laboratorio, $equipo, $fecha))->download('reportes_laboratorios.'.$tipo);
    }
}
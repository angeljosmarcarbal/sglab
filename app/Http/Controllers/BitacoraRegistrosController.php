<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class BitacoraRegistrosController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

    public function show(){

    	$usos = DB::table('usos')
            ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
            ->join('users', 'usos.id_usuario', '=', 'users.id')
            ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
            ->select('laboratorios.nombre', 'users.matricula', 'usos.*', 'computadoras.numero')
            ->get();
    	// return view('bitacora.usos.uso_maquinas');
    	return view('bitacora.usos.uso_maquinas')->with(compact('usos'));
    }
}

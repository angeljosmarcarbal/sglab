<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Laboratorio;
use App\Computadora;
use App\Uso;
use App\TipoIncidente;
use App\UsoIncidente;
//date_default_timezone_set('America/Mexico_City');

class AlumnoController extends Controller
{
    
    public function __construct(){
        $this->middleware('auth', ['only'=>'index']);
         $this->middleware('auth', ['only'=>'incidencias']);
    }
        
    /*Nuevo*/
    public function index(){
        /*Prueba dependientes*/
        $laboratorios = Laboratorio::where('status','=', 1)->with('iLaboratorios_all')->get();

        /*termina prueba dependientes*/
        $id_usuario= auth()->user()->id;
        $usuario = User::where('id', '<>', $id_usuario)->first();

        $computadoras= Computadora::all();
        /*$uso=new Uso();
        $uso->id_laboratiorio='0';
        $uso->id_usuario=$id_usuario;
        $uso->id_computadora='0';
        if($uso->save()) {
            return view('usuario.inicio')->with(compact('usuario','laboratorios', 'computadoras'));
        }else{
            return back()->with('mensajeError', 'Algo salio mal');
        }*/
        return view('usuario.inicio')->with(compact('usuario','laboratorios', 'computadoras'));
    }
    public function  altaUso(Request $request){

        $laboratorios = Laboratorio::all();
        $computadoras= Computadora::all();
        $tipoIncidencias = TipoIncidente::all();
        $prioridades = DB::table('prioridades')->where('status', 1)->get();
        $usuario_id = auth()->user()->id;
        $usoincidentes =  DB::table('uso_incidentes')
            ->where('usos.id_usuario', '=', $usuario_id)
            ->join('usos','uso_incidentes.id_uso', '=', 'usos.id_uso')
            ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
            ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
            ->join('status','uso_incidentes.id_TipoIncidente', '=', 'status.id' )
            ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
            ->select('laboratorios.nombre as laboratorio', 'computadoras.numero', 'uso_incidentes.detalles', 'status.nombre', 'status.status', 'prioridades.nombre as prioridad' )
            ->orderBy('uso_incidentes.created_at', 'desc')
            ->get();
        ;
        $sesion = DB::table('usos')->where('id_usuario', '=', $usuario_id)->get();
        $id_uso=$sesion->last()->id_uso;
        if($id_uso > 0){
            $sesionUpdate =
                DB::table('usos')
                    ->where('id_uso', '=', $id_uso)
                    ->update(array(
                        'id_laboratiorio' => $request->laboratorio,
                        'id_computadora' => $request->numero,
                        'created_at' => date('Y-m-d H:i:s')
                    ));
            if($sesionUpdate > 0){

                session(['uso' => true]);
                session(['usoDate' => date('Y-m-d H:i:s')]);

                $usoActual =  DB::table('usos')
                    ->where('usos.id_uso', '=', $id_uso)
                    ->join('users', 'usos.id_usuario', '=', 'users.id')
                    ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
                    ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
                    ->select('users.matricula', 'laboratorios.nombre', 'computadoras.numero', 'computadoras.marca', 'computadoras.modelo')
                    ->get();
                ;
                return view('usuario.incidencia')->with(compact( 'laboratorios', 'computadoras', 'tipoIncidencias', 'usoincidentes', 'usoActual', 'id_uso', 'prioridades'));
            }else{
                return back()->with('mensajeError', 'Algo salio mal');
            }
        }
    }

    public function  incidencias(Request $request){

        $laboratorios = Laboratorio::all();
        $computadoras= Computadora::all();
        $tipoIncidencias = TipoIncidente::all();
        $prioridades = DB::table('prioridades')->where('status', 1)->get();
        $usuario_id = auth()->user()->id;
         $sesion = DB::table('usos')->where('id_usuario', '=', $usuario_id)->get();
        $id_uso=$sesion->last()->id_uso;
        $usoincidentes =  DB::table('uso_incidentes')
            ->where('usos.id_usuario', '=', $usuario_id)
            ->join('usos','uso_incidentes.id_uso', '=', 'usos.id_uso')
            ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
            ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
            ->join('status','uso_incidentes.id_TipoIncidente', '=', 'status.id' )
            ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
            ->select('laboratorios.nombre as laboratorio', 'computadoras.numero', 'uso_incidentes.detalles', 'status.nombre', 'status.status', 'prioridades.nombre as prioridad' )
            ->orderBy('uso_incidentes.created_at', 'desc')
            ->get();
        ;
        $usoActual =  DB::table('usos')
                    ->where('usos.id_uso', '=', $id_uso)
                    ->join('users', 'usos.id_usuario', '=', 'users.id')
                    ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
                    ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
                    ->select('users.matricula', 'laboratorios.nombre', 'computadoras.numero', 'computadoras.marca', 'computadoras.modelo')
                    ->get();
        $sesion = DB::table('usos')->where('id_usuario', '=', $usuario_id)->get();
        $id_uso=$sesion->last()->id_uso;
        if($id_uso > 0){
                return view('usuario.incidencia')->with(compact( 'laboratorios', 'computadoras', 'tipoIncidencias', 'usoincidentes', 'usoActual', 'id_uso', 'prioridades'));
            }else{
                return back()->with('mensajeError', 'Algo salio mal');
        }
    }

    public function  altaIncidencias(Request $request){
        //dd($request->all());
        $incidencia=new UsoIncidente();
        $usuario_id = auth()->user()->id;
        $sesion = DB::table('usos')->where('id_usuario', '=', $usuario_id)->get();      
        $incidencia->id_uso = $request->uso;
        $incidencia->id_TipoIncidente=$request->incidencia;
        $incidencia->detalles=$request->detalles;
        $incidencia->id_prioridad = $request->prioridad;
        $incidencia->status='1';

        if($incidencia->save()){
            $laboratorios = Laboratorio::all();
            $computadoras= Computadora::all();
            $tipoIncidencias =  TipoIncidente::all();
            $prioridades = DB::table('prioridades')->where('status', 1)->get();
            $usuario_id = auth()->user()->id;
            $id_uso= $request->uso;
            $usoincidentes =  DB::table('uso_incidentes')
                ->where('usos.id_usuario', '=', $usuario_id)
                ->join('usos','uso_incidentes.id_uso', '=', 'usos.id_uso')
                ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
                ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
                ->join('status','uso_incidentes.id_TipoIncidente', '=', 'status.id' )
                ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
                ->select('laboratorios.nombre as laboratorio', 'computadoras.numero', 'uso_incidentes.detalles', 'status.nombre', 'status.status', 'prioridades.nombre as prioridad' )
                ->orderBy('uso_incidentes.created_at', 'desc')
                ->get();
            $sesion = DB::table('usos')->where('id_usuario', '=', $usuario_id)->get();
            $usoActual =  DB::table('usos')
                    ->where('usos.id_uso', '=', $id_uso)
                    ->join('users', 'usos.id_usuario', '=', 'users.id')
                    ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
                    ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
                    ->select('users.matricula', 'laboratorios.nombre', 'computadoras.numero', 'computadoras.marca', 'computadoras.modelo')
                    ->get();
                ;
            return view('usuario.incidencia')->with(compact( 'laboratorios', 'computadoras', 'tipoIncidencias', 'usoincidentes', 'usoActual', 'id_uso', 'prioridades'));
        }else{
            return back()->with('mensajeError', 'Algo salio mal');
        }
    }

    public function showPerfil(){
        $usuario = auth()->user();
        return view('usuario.perfil')->with(compact('usuario'));
    }

    public function bylaboratorio($id){
        //$valores = Computadora::where('laboratorio_id', $id)->get();
        //var_dump($valores);
       return Computadora::where('laboratorio_id', $id)->get();
    }
}

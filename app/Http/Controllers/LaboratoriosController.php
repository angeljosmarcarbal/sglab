<?php

namespace App\Http\Controllers;

use App;
Use Exception;
use Illuminate\Http\Request;

class LaboratoriosController extends Controller{

	public function __construct(){
		$this->middleware('auth');
	}

	public function show(){
		$laboratorios = App\laboratorio::all();
		return view('catalogos.laboratorios.laboratorios')->with(compact('laboratorios'));
	}

	public function nuevo(Request $request){
		//dd($request->all());
		$request->validate([
			'nombre_nuevo' => 'required'
		]);

		$laboratorio = new App\Laboratorio;
		$laboratorio->nombre = $request->nombre_nuevo;
		$laboratorio->status = 1;

		if ($laboratorio->save()) {
			return back()->with('mensaje', 'Laboratorio agregado');
		}
	}

	public function editar(Request $request){
		//dd($request->all());
		$request->validate([
			'nombre_editar' => 'required'
		]);

		$laboratorio = App\Laboratorio::findOrFail($request->laboratorio_id);
		$laboratorio->nombre = $request->nombre_editar;
		if(isset($request->status)){
			$laboratorio->status = 1;
		}else{
			$laboratorio->status = 0;
		}

		if($laboratorio->save()){
			return back()->with('mensaje', 'Laboratorio editado');
		}

	}

	public function eliminar(Request $request){
		//dd($request->all());
		$laboratorio = App\Laboratorio::findOrFail($request->laboratorio_id);
		try {
			$respuesta=$laboratorio->delete();
		} catch (Exception $e) {
			if($e->errorInfo[1]==1451){
				return back()->withErrors(['foreign' => 'El laboratorio no se puede eliminar por que hay datos relacionados con él']);
			}
		}
		if($respuesta ==true){
			return back()->with('mensaje', 'Laboratorio Eliminado');
		}
	}
   
}

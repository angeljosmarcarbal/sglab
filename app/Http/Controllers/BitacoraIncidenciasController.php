<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BitacoraIncidenciasController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

    public function show(){

        $usoincidentes =  DB::table('uso_incidentes')
            ->join('usos','uso_incidentes.id_uso', '=', 'usos.id_uso')
            ->join('users','usos.id_usuario', '=', 'users.id')
            ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
            ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
            ->join('status','uso_incidentes.id_TipoIncidente', '=', 'status.id' )
            ->select('uso_incidentes.id', 'users.matricula','laboratorios.nombre as laboratorio', 'computadoras.numero', 'uso_incidentes.detalles', 'status.nombre', 'status.id' )
            ->orderBy('uso_incidentes.created_at', 'desc')
            ->get();
        $status = DB::table('status')->get();
    	return view('bitacora.incidencias.incidencias')->with(compact('usoincidentes', 'status'));
    }
    public function updateEstado(Request $request){

        $sesionUpdate =
            DB::table('uso_incidentes')
                ->where('id', '=', $request->id)
                ->update(array(
                    'id_TipoIncidente' => $request->status
                ));

        if(count($sesionUpdate)>0){
            $status = Status::all();
            $usoincidentes =  DB::table('uso_incidentes')
                ->join('usos','uso_incidentes.id_uso', '=', 'usos.id_uso')
                ->join('users','usos.id_usuario', '=', 'users.id')
                ->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
                ->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
                ->join('status','uso_incidentes.id_TipoIncidente', '=', 'status.id' )
                ->select('uso_incidentes.id', 'users.matricula','laboratorios.nombre as laboratorio', 'computadoras.numero', 'uso_incidentes.detalles', 'status.nombre', 'status.status' )
                ->orderBy('uso_incidentes.created_at', 'desc')
                ->get();
            return view('bitacora.incidencias.incidencias')->with(compact('usoincidentes', 'status'));
            }else{
                return back()->with('mensajeError', 'Algo salio mal');
            }

    }
}

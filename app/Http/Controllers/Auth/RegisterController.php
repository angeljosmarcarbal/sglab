<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function register(Request $request){
        //dd($request->all());
        $request->validate([
            'nombre' => 'required|string',
            'apellido_paterno' => 'required|string',
            'apellido_materno' => 'required|string',
            'telefono' => 'required|numeric|digits:10',
            'id_docente' => 'required|string',
            'correo' => 'required|email|max:255|unique:users,email',
            'password' => 'required|string|max:50',
            /*'confirmar_password' => 'required|string|max:50'*/
        ]);

        $user = new User;
        $user->nombre = $request->nombre;
        $user->apellido_paterno = $request->apellido_paterno;
        $user->apellido_materno = $request->apellido_materno;
        $user->email = $request->correo;
        $user->matricula = $request->id_docente;
        $user->password = bcrypt($request->password);
        $user->telefono = $request->telefono;
        $user->rol_id = 2;
        $user->status = 1;
        $user->remember_token = null;
        $user->created_at = date('Y-m-d H:m:i');
        $user->updated_at = date('Y-m-d H:m:i');

        if($user->save()){
            return redirect()->route('login')->withErrors(['saveOk' => 'Sus datos se guararon correctamente. Intente iniciar sesión', 'tipo' => 'docente']);
        }
        return back()->withErrors(['saveError' => 'No pudimos guardar sus datos. Intente más tarde']);
    }
}

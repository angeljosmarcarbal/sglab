<?php

namespace App\Http\Controllers\Auth;
use App\User;
use App\Uso;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm(){
        return view('auth.login');
    }

    public function __construct(){
        $this->middleware('guest', ['only'=>'showLoginForm']);
    }

    public function login(Request $request){
        /*$credentials = $this->validate(request(),[
            $this->username() => 'required|string|min:1',
            'password' => 'required|string|min:1',
            'tipo' => 'required'
        ]);*/

        if(strlen(trim($request->{$this->username()})) == 0 && strlen(trim($request->password)) == 0)
            return back()->withErrors([$this->username() => 'Este campo es requerido', 'password' => 'Este campo es requerido', 'tipo' => $request->tipo ])->withInput();
        if(strlen(trim($request->{$this->username()})) == 0 && strlen(trim($request->password)) > 0)
            return back()->withErrors([$this->username() => 'Este campo es requerido', 'tipo' => $request->tipo ])->withInput();
        if(strlen(trim($request->{$this->username()})) > 0 && strlen(trim($request->password)) == 0)
            return back()->withErrors(['password' => 'Este campo es requerido', 'tipo' => $request->tipo ])->withInput();

        $credentials = [
            $this->username() => $request->matricula,
            'password' => $request->password
        ];

        $matricula = $request->matricula;
        $password = $request->password;
        if(Auth::attempt($credentials)) {
            if(auth()->user()->rol_id==1){
                return redirect()->route('usuarios.show') ;
            }
            if(auth()->user()->rol_id==2){

                $uso = new Uso();
                $uso->id_laboratiorio = '0';
                $uso->id_usuario = auth()->user()->id;
                $uso->id_computadora = '0';
                $uso->created_at = null;
                $uso->updated_at = null;

                if($uso->save()) {
                    session(['uso' => false]);
                    return redirect()->route('index');
                }

                return redirect()->route('index')->withErrors(['msgError' => 'Algo salió mal. Intente iniciar sesión de nuevo']);
                
            }
        }else{
            $params = array('lsMatricula' => $matricula, 'lsPassword' => $password);
            $client = new \nusoap_client('http://www.mi-escuelamx.com/ws/wsUTSEM/Datos.asmx?wsdl', true);
            $client->soap_defencoding = 'UTF-8';
            $client->decode_utf8 = false;
            $result = $client->call("Login", $params);
            //var_dump($result);/*
            if(count($result)>0){
                foreach ($result as $row) {
                    if ($row['diffgram'] != null) {
                        foreach ($row['diffgram'] as $st) {
                            $stCareer = $st['Alumno']['desc_carrera'];
                            $stActive = $st['Alumno']['desc_situacion'];
                            //Verificamos que el alumno este activo
                            if ($stActive == 'Activo') {
                                //Consulta a la BD para saver si ya existe
                                $Users = DB::table('users')
                                    ->select('*')
                                    ->where('status', 1)
                                    ->where('matricula', $matricula)
                                    ->get();
                                if (count($Users) > 0) {
                                    foreach ($Users as $User) {
                                        if (Hash::check($password, $User->password)) {
                                            if (Auth::attempt(['enrollment' => $this->username(), 'password' => $password])) {
                                                if(auth()->user()->rol_id==1){
                                                    return redirect()->route('usuarios.show') ;
                                                }
                                                if(auth()->user()->rol_id==2){

                                                    $uso = new Uso();
                                                    $uso->id_laboratiorio = '0';
                                                    $uso->id_usuario = auth()->user()->id;
                                                    $uso->id_computadora = '0';
                                                    $uso->created_at = null;
                                                    $uso->updated_at = null;

                                                    if($uso->save()) {
                                                        session(['uso' => false]);
                                                        return redirect()->route('index');
                                                    }

                                                    return redirect()->route('index')->withErrors(['msgError' => 'Algo salió mal. Intente iniciar sesión de nuevo']);

                                                }
                                            }
                                        } else {
                                            return back()->withErrors(['password' => 'La contraseña no es correcta']);
                                        }
                                    }
                                }else{
                                    $usuario=new User();
                                    $usuario->matricula=$request->matricula;
                                    $usuario->nombre=$st['Alumno']['nombre'];
                                    $usuario->apellido_paterno=$st['Alumno']['apaterno'];
                                    $usuario->apellido_materno=$st['Alumno']['amaterno'];
                                    $usuario->email=$st['Alumno']['mail'];
                                    $usuario->telefono='0000000000';
                                    $usuario->rol_id='2';
                                    $usuario->password=bcrypt($password);
                                    $usuario->status=1;
                                    $usuario->save();
                                    //Una vez registrado se procede a autenticarlo
                                    if (Auth::attempt(['matricula' => $matricula, 'password' => $password])) {
                                        if(auth()->user()->rol_id==1){
                                            return redirect()->route('usuarios.show') ;
                                        }
                                        if(auth()->user()->rol_id==2){

                                            $uso = new Uso();
                                            $uso->id_laboratiorio = '0';
                                            $uso->id_usuario = auth()->user()->id;
                                            $uso->id_computadora = '0';
                                            $uso->created_at = null;
                                            $uso->updated_at = null;

                                            if($uso->save()) {
                                                session(['uso' => false]);
                                                return redirect()->route('index');
                                            }else{
                                                return redirect()->route('index')->withErrors(['msgError' => 'Algo salió mal. Intente iniciar sesión de nuevo']);
                                            }
                                           
                                        }
                                    }else{
                                        return view('auth.login')->withErrors(['matricula' => 'Datos incorrectos', 'password' => 'Datos incorrectos', 'tipo' => $request->tipo]);
                                    }
                                }
                            }else{
                                return view('auth.login')->withErrors(['matricula' => 'La matricula que ingresó es de un alumno egresado.', 'tipo' => $request->tipo]);
                            }
                        }
                    }else{
                        return view('auth.login')->withErrors(['matricula' => 'Datos incorrectos', 'password' => 'Dato incorrectos', 'tipo' => $request->tipo]);   
                    }
                }
            }else{
                return back()->with('mensajeError', 'No se puede conectar');
            }
        }
    }

    public function LogFin() {
        //date_default_timezone_set('America/Mexico_City');
        $usuario = auth()->user()->id;
        $uso = DB::table('usos')->where('id_usuario', '=', $usuario)->get();
        if(count($uso) > 0){
            $usoFin =
                DB::table('usos')
                    ->where('id_uso', '=', $uso->last()->id_uso)
                    ->update(array(
                        'updated_at' => date('Y-m-d H:i:s')
                    ));
            $usofinal = DB::table('usos')->where('id_uso', '=', $uso->last()->id_uso)->get();
            //Auth::logout();
        }
        Auth::logout();
        return redirect('/') ;

    }

    public function username(){
        return 'matricula';
    }




}

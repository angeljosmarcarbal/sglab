<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Hash;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller{

	public function __construct(){
		$this->middleware('auth');
	}

	public function show(){
		$usuarios = App\User::where('id', '<>', auth()->user()->id)->get();
		$roles = App\Role::all();
		return view('administracion.usuarios.usuarios')->with(compact('usuarios'))->with(compact('roles'));
	}
    public function show2(){
        $usuarios = App\User::where('id', '<>', auth()->user()->id)->get();
        $Laboratorios = App\Laboratorio::all();
        return view('administracion.usuarios.usuarios2')->with(compact('usuarios'))->with(compact('Laboratorios'));
    }

	public function showPerfil(){
		$usuario = auth()->user();
		return view('administracion.usuarios.perfil')->with(compact('usuario'));
	}
    
    public function nuevo(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'matricula' => 'required|string',
    		'nombre' => 'required|string|max:255',
    		'apellido_paterno' => 'required|string|max:255',
    		'apellido_materno' => 'required|string|max:255',
    		'correo' => 'required|email|unique:users,email',
    		'telefono' => 'required|numeric|min:10',
    		'rol' => 'required',
    		//'password' => 'required|string'
    	]);
    	$usuario=new App\User;
    	$usuario->matricula=$request->matricula;
    	$usuario->nombre=$request->nombre;
    	$usuario->apellido_paterno=$request->apellido_paterno;
    	$usuario->apellido_materno=$request->apellido_materno;
    	$usuario->email=$request->correo;
    	$usuario->telefono=$request->telefono;
    	$usuario->rol_id=$request->rol;
    	$usuario->password=bcrypt("12345");
    	$usuario->status=1;
    	if($usuario->save()){
    		return back()->with('mensaje', 'Usuario agregado');
    	}else{
    		return back()->with('mensajeError', 'No se pudo agregar');
    	}
    }

    public function editar(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'usuario_id' => 'required',
    		'matricula_editar' => 'required|string',
    		'nombre_editar' => 'required|string|max:255',
    		'apellido_paterno_editar' => 'required|string|max:255',
    		'apellido_materno_editar' => 'required|string|max:255',
    		'correo_editar' => 'required|email',
    		'telefono_editar' => 'required|numeric|min:10',
    		'rol_editar' => 'required',
    	]);
    	$usuario=App\User::findOrFail($request->usuario_id);
    	$nemail = [
    		'matricula' => $request->matricula_editar,
    		'nombre' => $request->nombre_editar,
    		'apellido_paterno' => $request->apellido_paterno_editar,
    		'apellido_materno' => $request->apellido_materno_editar,
    		'telefono' => $request->telefono_editar,
    		'rol_id' => $request->rol_editar,
    		//'password' => $request->password_editar==NULL?$usuario->password:bcrypt($request->password_editar),
    		'status' => isset($request->status_editar)?1:0
    	];
    	$cemail = [
    		'matricula' => $request->matricula_editar,
    		'nombre' => $request->nombre_editar,
    		'apellido_paterno' => $request->apellido_paterno_editar,
    		'apellido_materno' => $request->apellido_materno_editar,
    		'email' => $request->correo_editar,
    		'telefono' => $request->telefono_editar,
    		'rol_id' => $request->rol_editar,
    		//'password' => $request->password_editar==NULL?$usuario->password:bcrypt($request->password_editar),
    		'status' => isset($request->status_editar)?1:0
    	];
    	
    	$resultado=($usuario->email==$request->correo_editar?$usuario->update($nemail):$usuario->update($cemail));
    	if($resultado){
    		return back()->with('mensaje', 'Usuario modificado');
    	}else{
    		return back()->with('mensajeError', 'No se pudo modificar');
    	}
    }

    public function eliminar(Request $request){
    	//dd($request->all());
    	$usuario=App\User::findOrFail($request->usuario_id);
        $usos = DB::table('usos')->where('id_usuario', $request->usuario_id)->get();
        if ($usos->count() > 0) {
            return back()->withErrors(['foreign' => 'No se puede eliminar. Hay registros en bitácoras con este usuario.']);
        }else{
            if($usuario->delete()){
                return back()->with('mensaje', 'Usuario eliminado');
            }else{
                return back()->with('mensajeError', 'No se pudo eliminar');
            }
        }
    }

    public function passwordCambiar(Request $request){
        //dd($request->all());
        $request->validate([
            'password_anterior' => "required|string",
            'password_nueva' => "required|string",
            'password_nueva_repetir' => "required|string"
        ]);
        if(!Hash::check($request->password_anterior, auth()->user()->password, [])){
            return back()->withErrors(['password_anterior' => 'La contraseña no coincide con la actual'])->withInput();
        }
        if($request->password_nueva != $request->password_nueva_repetir){
            return back()->withErrors(['password_nueva_repetir' => 'La contraseña no coincide con la nueva'])->withInput();
        }
        $user = App\User::findOrFail(auth()->user()->id);
        $user->password = bcrypt($request->password_nueva);
        if ($user->save()) {
            $message= ['password_success' => 'Tu contraseña se cambió'];
            return redirect()->back()->withErrors($message);
        }
    }
}

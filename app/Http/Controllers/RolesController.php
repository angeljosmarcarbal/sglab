<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class RolesController extends Controller{

	protected $redirectTo = "login";

	public function __construct(){
		$this->middleware('auth');
	}

	public function show(){
		$areas = App\Area::where('status', 1)->get();
		$roles = App\Role::all();
		return view('administracion.roles.roles')->with(compact('areas'))->with(compact('roles'));
	}

	public function nuevo(Request $request){
		//dd($request);
		$request->validate([
			'nombre' => 'required',
			'areas'  => 'required'
		]);

		$rol = new App\Role;
		$rol->nombre = $request->nombre;
		$rol->areas  = implode(',', $request->areas);
		$rol->eliminable = 1;
		$rol->status = 1;

		if($rol->save()){
			$mensaje = 'Rol guardado';
		}else{
			$mensaje = 'No se pudo guardar el rol';
		}

		return back()->with('mensaje', $mensaje);
	}

	public function modificar(Request $request){
		/*$request->validate([
			'rol_id' => 'required',
			'nombre-mod' => 'required',
			'areas-mod'  => 'required'
		]);*/
		$rol = App\Role::findOrFail($request->rol_id);
		$rol->nombre = $request->nombre;
		$rol->areas = implode(',', $request->areas);
		if (isset($request->status)) {
			$rol->status = 1;
		}else{
			$rol->status = 0;
		}

		if($rol->save()){
			$mensaje = 'Rol modificado';
		}else{
			$mensaje = 'No se pudo modificado el rol';
		}

		return back()->with('mensaje', $mensaje);
	}

	public function eliminar(Request $request){
		$rol = App\Role::findOrFail($request->rol_id);
		try {
		     $rol->delete();
		     return back()->with('mensaje', 'Rol eliminado');
		}catch (\Illuminate\Database\QueryException $e) {
		    if($e->getCode() == "23000"){ //23000 is sql code for integrity constraint violation
		        return back()->withErrors(['foreign' => 'Este rol está en uso. No se puede eliminar']);
		    }
		}
	}
    
}

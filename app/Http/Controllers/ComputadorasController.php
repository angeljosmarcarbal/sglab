<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComputadorasController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

	public function show($idLaboratorio){
		$computadoras = App\Computadora::where('laboratorio_id', $idLaboratorio)->get();
        $laboratorios = App\Laboratorio::where('id', $idLaboratorio)->get();
		return view('catalogos.computadoras.computadoras')->with(compact('idLaboratorio'))->with(compact('computadoras'))->with(compact('laboratorios'));
	}
    
    public function nuevo(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'numero_nuevo' => 'required|integer|min:1',
    		'marca_nuevo' => 'required',
    		'modelo_nuevo' => 'required'	
    	]);

    	$computadora = new App\Computadora;
    	$computadora->laboratorio_id = $request->laboratorio_id;
    	$computadora->numero = $request->numero_nuevo;
    	$computadora->marca = $request->marca_nuevo;
    	$computadora->modelo = $request->modelo_nuevo;
    	$computadora->status = 1;

    	if($computadora->save()){
    		return back()->with('mensaje', 'Computadora agregada');
    	}
    }

    public function editar(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'numero_editar' => 'required|integer|min:1',
    		'marca_editar' => 'required',
    		'modelo_editar' => 'required'	
    	]);

    	$computadora = App\Computadora::findOrFail($request->computadora_id);
    	$computadora->laboratorio_id = $request->laboratorio_id;
    	$computadora->numero = $request->numero_editar;
    	$computadora->marca= $request->marca_editar;
    	$computadora->modelo =$request->modelo_editar;
    	if (isset($request->status)) {
    		$computadora->status =1;
    	}else{
    		$computadora->status=0;
    	}

    	if ($computadora->save()) {
    		return back()->with('mensaje', 'Computadora editada');
    	}
    }

    public function eliminar(Request $request){
        //dd($request->all());
        $computadora = App\Computadora::findOrFail($request->computadora_id);
        $usos = DB::table('usos')->where('id_computadora', $request->computadora_id)->get();
        if ($usos->count()>0) {
            return back()->withErrors(['foreign' => 'No se puede eliminar este equipo']);
        }
        if($computadora->delete()){
            return back()->with('mensaje', 'Computadora eliminada');
        }
    }


}

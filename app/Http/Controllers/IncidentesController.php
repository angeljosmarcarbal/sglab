<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IncidentesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function show(){
    	$incidentes = App\TipoIncidente::all();
    	return view('catalogos.incidentes.incidentes')->with(compact('incidentes'));
    }

    public function nuevo(Request $request){
    	//dd($request->all());
    	$request->validate([
            'tipo_nuevo' => 'required',
    		'nombre_nuevo' => 'required',
            'descripcion_nuevo' => 'required'
    	]);

    	$incidente = new App\TipoIncidente;
        $incidente->tipo = $request->tipo_nuevo;
    	$incidente->nombre = $request->nombre_nuevo;
        $incidente->descripcion = $request->descripcion_nuevo;
    	$incidente->status = 1;
    	if ($incidente->save()) {
    		return back()->with('mensaje', 'Tipo de incidente agregado');
    	}
    }

    public function editar(Request $request){
    	//dd($request->all());
        $request->validate([
            'tipo_editar' => 'required',
            'nombre_editar' => 'required',
            'descripcion_editar' => 'required'
        ]);
    	$incidente = App\TipoIncidente::findOrFail($request->incidente_id);
        $incidente->tipo = $request->tipo_editar;
    	$incidente->nombre = $request->nombre_editar;
        $incidente->descripcion = $request->descripcion_editar;
    	if(isset($request->status)){
    		$incidente->status = 1;
    	}else{
    		$incidente->status = 0;
    	}

    	if ($incidente->save()) {
    		return back()->with('mensaje', 'Tipo de incidente actualizado');
    	}
    }

    public function eliminar(Request $request){
    	//dd($request->all());
    	$incidente = App\TipoIncidente::findOrFail($request->incidente_id);
        $usos = DB::table('uso_incidentes')->where('id_TipoIncidente', $request->incidente_id)->get();
        if($usos->count()>0){
            return back()->withErrors(['foreign' => 'No se puede eliminar']);
        }
    	if ($incidente->delete()) {
    		return back()->with('mensaje', 'Tipo de incidente eliminado');
    	}
    }
}

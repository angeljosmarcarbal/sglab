<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class PrioridadesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function show(){
    	$prioridades = App\Prioridad::all();
    	return view('catalogos.prioridades.prioridades')->with(compact('prioridades'));
    }

    public function nuevo(Request $request){
    	$request->validate([
            'tipo_nuevo' => 'required',
    		'nombre_nuevo' => 'required',
            'descripcion_nuevo' => 'required'
    	]);

    	$prioridad = new App\Prioridad;
        $prioridad->tipo = $request->tipo_nuevo;
    	$prioridad->nombre = $request->nombre_nuevo;
        $prioridad->descripcion = $request->descripcion_nuevo;
    	$prioridad->status = 1;

    	if ($prioridad->save()) {
    		return back()->with('mensaje', 'Prioridad guardada');
    	}
    }

    public function modificar(Request $request){
    	//dd($request->all());
    	$request->validate([
            'tipo_editar' => 'required',
    		'nombre_editar' => 'required',
            'descripcion_editar' => 'required'
    	]);

    	$prioridad = App\Prioridad::findOrFail($request->prioridad_id);
        $prioridad->tipo = $request->tipo_editar;
    	$prioridad->nombre = $request->nombre_editar;
        $prioridad->descripcion = $request->descripcion_editar;
    	if(isset(($request->status))){
    		$prioridad->status = 1;
    	}else{
    		$prioridad->status = 0;
    	}

    	if ($prioridad->save()) {
    		return back()->with('mensaje', 'Prioridad actualizada');
    	}
    }

    public function eliminar(Request $request){
    	//dd($request->all());
    	$prioridad = App\Prioridad::findOrFail($request->prioridad_id);

    	if ($prioridad->delete()) {
    		return back()->with('mensaje', 'Prioridad eliminada');
    	}
    }
}

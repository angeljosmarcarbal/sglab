<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class StatusController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function show(){
    	$status = App\Status::all(); 
    	return view('catalogos.status.status')->with(compact('status'));
    }

    public function nuevo(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'nombre_nuevo' => 'required',
            'atendido_por_nuevo' => 'required',
            'descripcion_nuevo' =>  'required'
    	]);

    	$status = new App\Status;
    	$status->nombre = $request->nombre_nuevo;
        $status->atendido_por = $request->atendido_por_nuevo;
        $status->descripcion = $request->descripcion_nuevo;
    	$status->status = 1;

    	if ($status->save()) {
    		return back()->with('mensaje', 'Status agregado');
    	}
    }

    public function editar(Request $request){
    	//dd($request->all());
        $request->validate([
            'nombre_editar' => 'required',
            'atendido_por_editar' => 'required',
            'descripcion_editar' =>  'required'
        ]);
    	$status = App\Status::findOrFail($request->status_id);
    	$status->nombre = $request->nombre_editar;
        $status->atendido_por = $request->atendido_por_editar;
        $status->descripcion = $request->descripcion_editar;
    	if(isset($request->status)){
    		$status->status = 1;
    	}else{
    		$status->status =0;
    	}

    	if ($status->save()) {
    		return back()->with('mensaje', 'Status modificado');
    	}
    }

    public function eliminar(Request $request){
    	//dd($request->all());
    	$status = App\Status::findOrFail($request->status_id);
    	if ($status->delete()) {
    		return back()->with('mensaje', 'Status eliminado');
    	}
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
    public function iLaboratorios_all(){
        return $this->hasMany('App\Computadora', 'laboratorio_id');
    }
}

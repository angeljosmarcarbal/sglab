<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoIncidente extends Model
{
    protected $table = "tipos_de_incidentes";
}

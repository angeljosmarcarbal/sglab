<?php

namespace App\Exports;

use Illuminate\Support\Facades\DB;
use App\Laboratorio;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;

class ReportesExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    use Exportable;

    public $laboratorio;
    public $equipo;
    public $date;

    public function __construct($laboratorio, $equipo, $date=''){
    	$this->laboratorio = $laboratorio;
    	$this->equipo = $equipo;
    	$this->date = $date;
    }

    public function view(): View
    {
    	if($this->date == ''){
    		$array = [
    						['usos.id_laboratiorio', $this->laboratorio],
    						['usos.id_computadora', $this->equipo]
    					];
    	}else{
    		$array = [
    						['usos.id_laboratiorio', $this->laboratorio],
    						['usos.id_computadora', $this->equipo],
    						[DB::raw('date(uso_incidentes.created_at)'), $this->date]
    					];
    	}
    	$datos = DB::table('uso_incidentes')->select('uso_incidentes.id', 
						'laboratorios.nombre as laboratorio', 
						DB::raw("concat(computadoras.numero,' ',computadoras.marca,' ',computadoras.modelo) as equipo"), 
						DB::raw("concat(users.nombre,' ',users.apellido_paterno,' ',users.apellido_materno) as usuario"), 
						'tipos_de_incidentes.nombre as incidente',
						'uso_incidentes.detalles as comentario',
                        'prioridades.nombre as prioridad', 
						'status.nombre as status',
						'uso_incidentes.created_at as fecha')
    					->join('usos', 'uso_incidentes.id_uso', '=', 'usos.id_uso')
    					->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
    					->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
    					->join('users', 'usos.id_usuario', '=', 'users.id')
    					->join('tipos_de_incidentes', 'uso_incidentes.id_TipoIncidente', '=', 'tipos_de_incidentes.id')
    					->join('status', 'uso_incidentes.status', '=', 'status.id')
                        ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
    					->where($array)->get();
    	return view('exports.export', compact('datos'));
    }

   /* public function collection()
    {
        return $datos = DB::table('uso_incidentes')->select('uso_incidentes.id', 
						'laboratorios.nombre as laboratorio', 
						DB::raw("concat(computadoras.numero,' ',computadoras.marca,' ',computadoras.modelo) as equipo"), 
						DB::raw("concat(users.nombre,' ',users.apellido_paterno,' ',users.apellido_materno) as usuario"), 
						'tipos_de_incidentes.nombre as incidente',
						'uso_incidentes.detalles as comentario',
                        'prioridades.nombre as prioridad', 
						'status.nombre as status',
						'uso_incidentes.created_at as fecha')
    					->join('usos', 'uso_incidentes.id_uso', '=', 'usos.id_uso')
    					->join('laboratorios', 'usos.id_laboratiorio', '=', 'laboratorios.id')
    					->join('computadoras', 'usos.id_computadora', '=', 'computadoras.id')
    					->join('users', 'usos.id_usuario', '=', 'users.id')
    					->join('tipos_de_incidentes', 'uso_incidentes.id_TipoIncidente', '=', 'tipos_de_incidentes.id')
    					->join('status', 'uso_incidentes.status', '=', 'status.id')
                        ->join('prioridades', 'uso_incidentes.id_prioridad', '=', 'prioridades.id')
    					->where([
    						['usos.id_laboratiorio', $this->laboratorio],
    						['usos.id_computadora', $this->equipo]
    					])->get();
    }*/
}

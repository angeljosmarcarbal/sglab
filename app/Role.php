<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    //

    public function getAreas(){
    	$areas = explode(',', $this->areas);
    	$names = '';
    	for($i = 0; $i < count($areas); $i++) {
    		$db = DB::table('areas')->select('nombre')->where('id', $areas[$i])->get();
    		foreach ($db as $name) {
    			$names .= $name->nombre.', ';
    		}
    	}
    	return $names;
    }
}

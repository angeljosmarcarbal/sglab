<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Area;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'matricula', 
        'nombre', 
        'apellido_paterno', 
        'apellido_materno', 
        'email', 
        'password', 
        'telefono', 
        'status',
        'rol_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tieneArea($rol=null, $area=null){
        $areasResult=Role::where('id', '=', $rol)->get();
        $areas=explode(',', $areasResult[0]->areas);
        if(in_array($area, $areas)) return true; 
        return false;
    }
}

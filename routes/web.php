<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Auth\LoginController@showLoginForm');

/*Roles*/

Route::get('/roles', 'RolesController@show')->name('roles.show');
Route::post('/roles/nuevo', 'RolesController@nuevo')->name('roles.nuevo');
Route::post('/roles/modificar', 'RolesController@modificar')->name('roles.modificar');
Route::post('/roles/eliminar', 'RolesController@eliminar')->name('roles.eliminar');

/*Usuarios*/



Route::get('/usuarios', 'UsuariosController@show')->name('usuarios.show');
Route::get('/perfil', 'UsuariosController@showPerfil')->name('perfil');
Route::post('/usuarios/nuevo', 'UsuariosController@nuevo')->name('usuarios.nuevo');
Route::post('/usuarios/editar', 'UsuariosController@editar')->name('usuarios.editar');
Route::post('/usuarios/eliminar', 'UsuariosController@eliminar')->name('usuarios.eliminar');
Route::post('/usuarios/password/cambiar', 'UsuariosController@passwordCambiar')->name('usuarios.password');

/*Laboratorios*/
Route::get('/laboratorios', 'LaboratoriosController@show')->name('laboratorios.show');
Route::post('/laboratorios/nuevo', 'LaboratoriosController@nuevo')->name('laboratorios.nuevo');
Route::post('/laboratorios/editar', 'LaboratoriosController@editar')->name('laboratorios.editar');
Route::post('/laboratorios/eliminar', 'LaboratoriosController@eliminar')->name('laboratorios.eliminar');

/*Computadoras*/
Route::get('/laboratorio/{idLaboratorio}/computadoras', 'ComputadorasController@show')->name('computadoras.show');
Route::post('/computadoras/nuevo', 'ComputadorasController@nuevo')->name('computadoras.nuevo');
Route::post('/computadoras/editar', 'ComputadorasController@editar')->name('computadoras.editar');
Route::post('/computadoras/eliminar', 'ComputadorasController@eliminar')->name('computadoras.eliminar');

/*Incidentes*/
Route::get('/incidentes', 'IncidentesController@show')->name('incidentes.show');
Route::post('/incidentes/nuevo', 'IncidentesController@nuevo')->name('incidentes.nuevo');
Route::post('/incidentes/editar', 'IncidentesController@editar')->name('incidentes.editar');
Route::post('/incidentes/eliminar', 'IncidentesController@eliminar')->name('incidentes.eliminar');

/*Prioridades*/
Route::get('/prioridades', 'PrioridadesController@show')->name('prioridades.show');
Route::post('/prioridades/nuevo', 'PrioridadesController@nuevo')->name('prioridades.nuevo');
Route::post('/prioridades/modificar', 'PrioridadesController@modificar')->name('prioridades.modificar');
Route::post('/prioridades/eliminar', 'PrioridadesController@eliminar')->name('prioridades.eliminar');

/*Status*/
Route::get('/status', 'StatusController@show')->name('status.show');
Route::post('/status/nuevo', 'StatusController@nuevo')->name('status.nuevo');
Route::post('/status/editar', 'StatusController@editar')->name('status.editar');
Route::post('/status/eliminar', 'StatusController@eliminar')->name('status.eliminar');

/*Bitaora Uso de maquinas*/
Route::get('/bitacora/registro/usos', 'BitacoraRegistrosController@show')->name('bitacora.registros.show');

/*Bitacora de incidentes*/
Route::get('/bitacora/registro/incidencias', 'BitacoraIncidenciasController@show')->name('bitacora.incidencias.show');
Route::post('/bitacora/registro/incidencias/update', 'BitacoraIncidenciasController@updateEstado')->name('updateEstado');

/*Login de usuarios de sistema*/

//Route::get('login', 'Auth\LoginController@index')->name('login');
//Route::post('login', 'Auth\LoginController@login')->name('login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');
//Route::post('logout', 'Auth\LoginController@LogFin')->name('logout');

/*-Usuarios-*/
Route::get('/inicio', 'AlumnoController@index')->name('index');
Route::post('/altaUso', 'AlumnoController@altaUso')->name('altaUso');
Route::get('/incidencias', 'AlumnoController@incidencias')->name('incidencias');
Route::post('/altaIncidencias', 'AlumnoController@altaIncidencias')->name('altaIncidencias');
Route::get('/perfilA', 'AlumnoController@showPerfil')->name('alumnoPerfil');
Route::post('/LogFin', 'Auth\LoginController@LogFin')->name('LogFin');

// Authentication Routes...

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
//Route::post('logout', 'Auth\LoginController@logout')->name('logout');
//Route::post('logout', 'UsuariosController@LogFin')->name('updateUso');


// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/*Reportes*/
Route::get('/reportes/equipos/incidencias', 'ReportesController@showReporteIncidencias')->name('reportes.reporte1');
Route::get('/reportes/equipos/incidencias/fecha', 'ReportesController@showReporteIncidenciasFecha')->name('reportes.reporte2');
Route::post('/reportes/laboratorio/maquinas', 'ReportesController@equiposPorMaquina')->name('reportes.em');
Route::post('/reportes/data', 'ReportesController@getData')->name('reportes.data');

/*Exportar*/
Route::get('/reportes/exportar/{laboratorio}/{equipo}/{tipo}', 'ReportesController@export')->name('reportes.exportar');
Route::get('/reportes/exportar/{laboratorio}/{equipo}/{fecha}/{tipo}', 'ReportesController@exportF')->name('reportes.exportarF');
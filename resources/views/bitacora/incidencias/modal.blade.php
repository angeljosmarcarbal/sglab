<div class="modal fade" id="incidenciasModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="" method="POST" id="form-eliminar-rol">
                    {{csrf_field()}}
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Detalles</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="">Laboratorio</label>
                        <input type="text" class="form-control" readonly="" value="{{$usoincidente->laboratorio}}">
                      </div>
                      <div class="form-group">
                        <label for="">Número de máquina</label>
                        <input type="text" class="form-control" readonly="" value="23">
                      </div>
                      <div class="form-group">
                        <label for="">Usuario</label>
                        <input type="text" class="form-control" readonly="" value="Josmar Carbajal">
                      </div>
                      <div class="form-group">
                        <label for="">Fecha de registro</label>
                        <input type="text" class="form-control" readonly="" value="2020/10/09 07:00:00">
                      </div>
                      <div class="form-group">
                        <label for="">Comentario</label>
                        <input type="text" class="form-control" readonly="" value="No funciona xampp">
                      </div>
                      <div class="form-group">
                        <label for="">Status</label>
                        <input type="text" class="form-control" readonly="" value="En proceso">
                      </div>
                      <div class="form-group">
                        <label for="">Cambiar status</label>
                        <select name="" class="form-control" id="">
                          <option value="">Recibido</option>
                          <option value="">En proceso</option>
                          <option value="">Sin resolver</option>
                          <option value="">Resuelto</option>
                        </select>
                        <div class="form-group float-right mt-2">
                          <button type="button" class="btn btn-primary btn-sm">Cambiar</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="hidden" name="rol_id" id="rol_id2" value="">                    
                </div>
                </form>
              </div>
            </div>
          </div>
@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Registros de incidencias en maquinas</h1>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Registros hechos</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<table class="table" id="tabla-bitacora-incidencias">
							<thead>
								<th>Laboratorio</th>
								<th>Maquina</th>
								<th>Usuario</th>
								<th>Comentario</th>
								<th>Status</th>
								<th>acciones</th>
							</thead>
							<tbody>
							@foreach($usoincidentes as $usoincidente)
								<tr>
									<td>{{$usoincidente->laboratorio}}</td>
									<td>{{$usoincidente->numero}}</td>
									<td>{{$usoincidente->matricula}}</td>
									<td>{{$usoincidente->detalles}}</td>
									<td>{{$usoincidente->nombre}}</td>
									<td>
										<form action="{{ route('updateEstado') }}" method="POST" id="form-eliminar-rol">
											{{csrf_field()}}

											<input type="text" value="{{$usoincidente->id}}" name="id" hidden>
											<div class="form-group">
												<select name="status" id="status" class="form-control">
													<option value="">Seleccione un estado</option>
													@foreach($status as $estado)
														<option value="{{$estado->id}}">{{$estado->nombre}}</option>
													@endforeach()
												</select>
												<div class="form-group float-right mt-2">
													<button type="submit" class="btn btn-primary btn-sm">Cambiar</button>
												</div>
											</div>
										</form>
									</td>

								</tr>
							@endforeach()

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function(){
		$('#bitacora-general').addClass('active');
		$('#collapseBootstrapBitacora').addClass('show');
		$('#bitacora-item-incidencias').addClass('active');

		$('#tabla-bitacora-incidencias').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});
	});
</script>

@endsection()
@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Registros de uso de Equipos</h1>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Registros hechos</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<table class="table" id="tabla-bitacora-maquinas">
							<thead>
								<th>ID</th>
								<th>Laboratorio</th>
								<th>Equipo</th>
								<th>Usuario</th>
								<th>Fecha</th>
								<th>Hora inicio</th>
								<th>Hora cierre</th>
								<th></th>
							</thead>
							<tbody>
								@foreach($usos as $uso)
									<tr>
										<td>{{$uso->id_uso}}</td>
										<td>{{$uso->nombre}}</td>
										<td>{{$uso->numero}}</td>
										<td>{{$uso->matricula}}</td>										
										<td>{{substr( $uso->created_at, 0,10)}}</td>
										<td>{{substr( $uso->created_at, -8)}}</td>
										<td>{{substr($uso->updated_at, -8)}}</td>

										<td><button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#detallesUsoModal">Ver detalles</button>	</td>																
									</tr>
								@endforeach()
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('bitacora.usos.modal')

<script>
	$(document).ready(function(){
		$('#bitacora-general').addClass('active');
		$('#collapseBootstrapBitacora').addClass('show');
		$('#bitacora-item-maquinas').addClass('active');

		$('#tabla-bitacora-maquinas').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});
	});
</script>

@endsection()
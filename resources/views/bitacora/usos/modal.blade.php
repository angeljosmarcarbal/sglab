<div class="modal fade" id="detallesUsoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="" method="POST" id="form-eliminar-rol">
                    {{csrf_field()}}
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Detalles</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="">Laboratorio</label>
                        <input type="text" class="form-control" readonly="" value="Laboratorio CC1">
                      </div>
                      <div class="form-group">
                        <label for="">Número de máquina</label>
                        <input type="text" class="form-control" readonly="" value="23">
                      </div>
                      <div class="form-group">
                        <label for="">Usuario</label>
                        <input type="text" class="form-control" readonly="" value="Josmar Carbajal">
                      </div>
                      <div class="form-group">
                        <label for="">Fecha</label>
                        <input type="text" class="form-control" readonly="" value="2020/10/09">
                      </div>
                      <div class="form-group">
                        <label for="">Hora de inicio</label>
                        <input type="text" class="form-control" readonly="" value="12:34:00">
                      </div>
                      <div class="form-group">
                        <label for="">Hora de cierre</label>
                        <input type="text" class="form-control" readonly="" value="02:10:00">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="hidden" name="rol_id" id="rol_id2" value="">
                    <button type="button" class="btn btn-primary">Aceptar</button>
                </div>
                </form>
              </div>
            </div>
          </div>
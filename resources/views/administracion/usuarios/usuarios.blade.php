@extends('plantilla.layout')
@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Administración de Usuarios</h1>
</div>

<div class="alert alert-danger" id="foreign" style="display: none;">No se puede eliminar. Hay registros en bitácoras con este usuario.</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Usuarios agregados</h6>
				<button class="btn btn-sm btn-default" data-toggle="modal" data-target="#nuevoUsuarioModal">Agregar nuevo</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<table class="table" id="tabla-usuarios">
							<thead>
								<th>Nombre</th>
								<th>Email</th>
								<th>Matricula</th>
								<th>Teléfono</th>
								<th>Rol de usuario</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($usuarios as $usuario)
									<tr>
										<td>{{$usuario->nombre.' '.$usuario->apellido_paterno.' '.$usuario->apellido_materno}}</td>
										<td>{{$usuario->email}}</td>
										<td>{{$usuario->matricula}}</td>
										<td>{{$usuario->telefono}}</td>
										<td>
											@foreach($roles as $rol)
												@if($usuario->rol_id == $rol->id)
													{{$rol->nombre}}
												@endif()
											@endforeach()
										</td>
										<td>
											@if($usuario->status==1)
												Activo
											@else
												Inactivo
											@endif()
										</td>
										<td>
											<a href="javascript:void(0);" onclick="modificarUsuario({{$usuario}});">Modificar</a>
										</td>
									</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@include('administracion.usuarios.modal-nuevo')
@include('administracion.usuarios.modal-modificar')

<script>
	function modificarUsuario(usuario){
		console.log(usuario);
		$('#usuario_id').val(usuario.id);
		$('#usuario_id2').val(usuario.id);
		$('#matricula_editar').val(usuario.matricula);
		$('#nombre_editar').val(usuario.nombre);
		$('#apellido_paterno_editar').val(usuario.apellido_paterno);
		$('#apellido_materno_editar').val(usuario.apellido_materno);
		$('#correo_editar').val(usuario.email);
		$('#telefono_editar').val(usuario.telefono);
		$('#rol_editar').val(usuario.rol_id);
		$('#modificarUsuarioModal').modal('show');
		$('#status_editar').removeAttr('checked');
		$('#label-status').text('');
		if(usuario.status==1){
			$('#status_editar').attr('checked', true);
			$('#label-status').text('Usuario Activo');
		}else{
			$('#label-status').text('Usuario Inactivo');
		}
	}

	$(document).ready(function(){
		$('#admin-general').addClass('active');
		$('#collapseBootstrap').addClass('show');
		$('#admin-item-usuarios').addClass('active');

		$('#tabla-usuarios').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		$('#status_editar').click(function(){
			if ($(this).is(':checked')) $('#label-status').text('Usuario Activo');
			else $('#label-status').text('Usuario Inactivo');
		})

		 @if($errors->has('matricula') || $errors->has('nombre') || $errors->has('apellido_paterno') || $errors->has('apellido_materno') || $errors->has('correo') || $errors->has('telefono') || $errors->has('rol'))
		 	$('#rol').val({{old('rol')}});
            $('#nuevoUsuarioModal').modal('show');
        @endif()
        @if($errors->has('matricula_editar') || $errors->has('nombre_editar') || $errors->has('apellido_paterno_editar') || $errors->has('apellido_materno_editar') || $errors->has('correo_editar') || $errors->has('telefono_editar') || $errors->has('rol_editar'))
            $('#modificarUsuarioModal').modal('show');
        @endif()

        @if($errors->has('foreign'))
			$('#foreign').show();
		@endif()
	})
</script>

@endsection()
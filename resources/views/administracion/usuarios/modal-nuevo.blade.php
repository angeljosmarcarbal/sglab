<div class="modal fade" id="nuevoUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <form action="{{ route('usuarios.nuevo') }}" method="POST" id="form-nuevo-usuario">
                    {{csrf_field()}}
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" id="exampleModalLabelLogout">Agregar usuario</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="matricula">Matricula*</label>
                        <input type="text" class="form-control" name="matricula" id="matricula" value="{{old('matricula')}}" required="">
                          @if($errors->has('matricula'))
                            <!-- <span style="color: red;">{{$errors->first('matricula')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="nombre">Nombre*</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" value="{{old('nombre')}}" required="">
                        @if($errors->has('nombre'))
                            <!-- <span style="color: red;">{{$errors->first('nombre')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="apellido_paterno">Apellido Paterno*</label>
                        <input type="text" class="form-control" name="apellido_paterno" id="apellido_paterno" value="{{old('apellido_paterno')}}" required="">
                        @if($errors->has('apellido_paterno'))
                            <!-- <span style="color: red;">{{$errors->first('apellido_paterno')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="apellido_materno">Apellido Materno*</label>
                        <input type="text" class="form-control" name="apellido_materno" id="apellido_materno" value="{{old('apellido_materno')}}" required="">
                        @if($errors->has('apellido_materno'))
                            <!-- <span style="color: red;">{{$errors->first('apellido_materno')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                    </div>
                  </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="correo">Correo*</label>
                        <input type="mail" class="form-control" name="correo" id="correo" value="{{old('correo')}}" required="">
                        @if($errors->has('correo'))
                            <span style="color: red;">{{$errors->first('correo')}}</span>
                            <!-- <span style="color: red;">Revisa este dato</span> -->
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="telefono">Teléfono*</label>
                        <input type="text" class="form-control" name="telefono" id="telefono" value="{{old('telefono')}}" required="">
                        @if($errors->has('telefono'))
                            <!-- <span style="color: red;">{{$errors->first('telefono')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="rol">Rol de usuario*</label>
                        <select name="rol" id="rol" class="form-control">
                          <option value="">Seleccione un dato</option>
                          @foreach($roles as $rol)
                            <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                          @endforeach()
                        </select>
                        @if($errors->has('rol'))
                            <!-- <span style="color: red;">{{$errors->first('rol')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <!-- <div class="form-group">
                        <label for="password">Password*</label>
                      </div>
                      <div class="input-group">
                        <input type="password" class="form-control" id="password" name="password" required="">
                        <div class="input-group-append">
                          <span class="input-group-text bg-primary" style="border: none;" onclick="myFunction()"><i class="fa fa-eye"></i></span>
                        </div>
                      </div> -->
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </div>
              </div>
            </div>
          </form> 
          </div>

          <script>
            function myFunction() {
              var pw_ele = document.getElementById("password");
              if (pw_ele.type == "password") {
                pw_ele.type = "text";
              } else {
                pw_ele.type = "password";
              }
            }
          </script>
@extends('plantilla.layout')
@section('general-content')

	<div class="alert bg-primary" id="password_success" style="display: {{($errors->has('password_success') ? '' : 'none')}}">
		Tu contraseñá se cambió.
	</div>
	
	<div class="row">
		<div class="col-lg-8">
			<div class="card mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Perfil</h6>
				</div>
				<div class="card-body">
					<form action="">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control" value="{{$usuario->nombre}}">
						</div>
						<div class="form-group">
							<label for="apellido_paterno">Apellido paterno</label>
							<input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" value="{{$usuario->apellido_paterno}}">
						</div>
						<div class="form-group">
							<label for="apellido_materno">Apellido materno</label>
							<input type="text" name="apellido_materno" id="apellido_materno" class="form-control" value="{{$usuario->apellido_materno}}">
						</div>
						<div class="form-group">
							<label for="nombre">Email</label>
							<input type="text" name="email" id="email" class="form-control" value="{{$usuario->email}}">
						</div>
						<div class="form-group">
							<label for="nombre">Teléfono</label>
							<input type="text" name="telefono" id="telefono" class="form-control" value="{{$usuario->telefono}}">
						</div>
						<div class="form-group">
							<label for="nombre">Email</label>
							<input type="text" name="email" id="email" class="form-control" value="{{$usuario->email}}">
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="card mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Cambiar contraseña</h6>
				</div>
				<div class="card-body">
					<form action="{{ route('usuarios.password') }}" method="POST" id="form-change">
						{{csrf_field()}}
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label for="password-anterior">Contraseña anterior*</label>
									<input type="text" id="password_anterior" name="password_anterior" class="form-control" value="{{old('password_anterior')}}">
									@if($errors->has('password_anterior'))
										<span class="text-danger">{{ $errors->first('password_anterior') }}</span>
									@endif()
								</div>
								<div class="form-group">
									<label for="password-nueva">Contraseña nueva*</label>
									<input type="text" id="password_nueva" name="password_nueva" class="form-control" value="{{old('password_nueva')}}">
									@if($errors->has('password_nueva'))
										<span class="text-danger">{{ $errors->first('password_nueva') }}</span>
									@endif()
								</div>
								<div class="form-group">
									<label for="password-nueva-repetir">Repita la contraseña nueva*</label>
									<input type="text" id="password_nueva_repetir" name="password_nueva_repetir" class="form-control" value="{{old('password_nueva_repetir')}}">
									@if($errors->has('password_nueva_repetir'))
										<span class="text-danger">{{ $errors->first('password_nueva_repetir') }}</span>
									@endif()
								</div>
								<div class="div-botones float-right">
									<div class="btn-group">
										<button type="button" class="btn btn-secondary" id="boton-cancelar-nuevo-rol">Cancelar</button>
										<button type="submit" class="btn btn-primary">Cambiar</button>
									</div>	
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer text-gray">
					<small>Los campos con * son obligatorios</small>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function(){
			$('#item-perfil').addClass('active');
		});
	</script>

@endsection()

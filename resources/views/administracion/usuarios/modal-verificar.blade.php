<div class="modal fade" id="verificarUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <!-- <form action="{{ route('roles.eliminar') }}" method="POST" id="form-eliminar-rol"> -->
                    {{csrf_field()}}
                <div class="modal-header">
                  <h5 class="modal-title text-primary" id="exampleModalLabelLogout">Verificar usuario</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Se tiene que verificar que el usuario que intenta agregar exista en SICA-A. Por favor ingrese el numero de matricula del usuario</p>
                  <div class="form-group">
                    <label for="numMatriula">Matricula</label>
                    <input type="text" class="form-control" name="matricula" id="numMatriula" placeholder="ej. 2016P09">
                  </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                      <button type="submit" class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#nuevoUsuarioModal">Verificar</button>
                    </div>
                </div>
                <!-- </form> -->
              </div>
            </div>
          </div>
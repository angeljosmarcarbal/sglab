<div class="modal fade" id="modificarUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{ route('usuarios.editar') }}" method="POST" id="form-modificar-usuario">
                    {{csrf_field()}}
                    <input type="hidden" name="usuario_id" id="usuario_id" value="">
                <div class="modal-header">
                  <h5 class="modal-title text-primary" id="exampleModalLabelLogout">Modificar Usuario</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="matricula">Matricula*</label>
                        <input type="text" class="form-control" name="matricula_editar" id="matricula_editar" value="{{old('matricula_editar')}}" required="">
                          @if($errors->has('matricula_editar'))
                            <!-- <span style="color: red;">{{$errors->first('matricula')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="nombre">Nombre*</label>
                        <input type="text" class="form-control" name="nombre_editar" id="nombre_editar" value="{{old('nombre_editar')}}" required="">
                        @if($errors->has('nombre_editar'))
                            <!-- <span style="color: red;">{{$errors->first('nombre')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="apellido_paterno">Apellido Paterno*</label>
                        <input type="text" class="form-control" name="apellido_paterno_editar" id="apellido_paterno_editar" value="{{old('apellido_paterno_editar')}}" required="">
                        @if($errors->has('apellido_paterno_editar'))
                            <!-- <span style="color: red;">{{$errors->first('apellido_paterno')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="apellido_materno">Apellido Materno*</label>
                        <input type="text" class="form-control" name="apellido_materno_editar" id="apellido_materno_editar" value="{{old('apellido_materno_editar')}}" required="">
                        @if($errors->has('apellido_materno_editar'))
                            <!-- <span style="color: red;">{{$errors->first('apellido_materno')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                    </div>
                  </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label for="correo">Correo*</label>
                        <input type="mail" class="form-control" name="correo_editar" id="correo_editar" value="{{old('correo_editar')}}" required="">
                        @if($errors->has('correo_editar'))
                            <!-- <span style="color: red;">{{$errors->first('correo')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="telefono">Teléfono*</label>
                        <input type="text" class="form-control" name="telefono_editar" id="telefono_editar" value="{{old('telefono_editar')}}" required="">
                        @if($errors->has('telefono_editar'))
                            <!-- <span style="color: red;">{{$errors->first('telefono')}}</span> -->
                            <span style="color: red;">{{$errors->first('telefono_editar')}}</span>
                          @endif()
                      </div>
                      <div class="form-group">
                        <label for="rol">Rol de usuario*</label>
                        <select name="rol_editar" id="rol_editar" class="form-control" value="{{old('rol_editar')}}">
                          <option value="">Seleccione un dato</option>
                          @foreach($roles as $rol)
                            <option value="{{$rol->id}}">{{$rol->nombre}}</option>
                          @endforeach()
                        </select>
                        @if($errors->has('rol_editar'))
                            <!-- <span style="color: red;">{{$errors->first('rol')}}</span> -->
                            <span style="color: red;">Revisa este dato</span>
                            <script>$('#rol_editar').val({{old('rol_editar')}})</script>
                          @endif()
                      </div>
                      <!-- <div class="form-group">
                        <label for="password">Password*</label>
                      </div> -->
                      <!-- <div class="input-group">
                        <input type="password" class="form-control" id="password_editar" name="password_editar">
                        <div class="input-group-append">
                          <span class="input-group-text bg-primary" style="border: none;" onclick="myFunction()"><i class="fa fa-eye"></i></span>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="status_editar" name="status_editar">
                            <label class="custom-control-label" for="status_editar" id="label-status">Usuario activo</label>
                        </div> 
                      </div>
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarUsuarioModal">Eliminar</button>
                      <button type="submit" class="btn btn-primary">Modificar</button>
                    </div>
                </div>
                </form>
              </div>
            </div>
          </div>


          <div class="modal fade" id="eliminarUsuarioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{ route('usuarios.eliminar') }}" method="POST" id="form-eliminar-usuario">
                    {{csrf_field()}}
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>¿Está seguro de querer eliminar este usuario?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="hidden" name="usuario_id" id="usuario_id2" value="">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                </div>
                </form>
              </div>
            </div>
          </div>
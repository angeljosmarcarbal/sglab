@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Administración de Roles de Usuario</h1>
</div>
<div class="alert alert-danger" id="foreign" style="display: none;">No se puede eliminar el rol de usuario por que está en uso</div>

<div class="row">
	<div class="col-lg-8">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Roles agregados</h6>
				<button class="btn btn-sm btn-default" id="boton-agregar-nuevo-rol">Agregar nuevo</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<style>
							#tabla-roles_info{
								margin-top: 50px!important;
							}
						</style>
						<table class="table" id="tabla-roles">
							<thead>
								<th>Nombre</th>
								<th>Áreas permitidas (<b>del SISTEMA</b>)</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($roles as $rol)
								<tr>
									<td>{{$rol->nombre}}</td>
									<td>{{$rol->getAreas()}}</td>
									<td>
										{{ ($rol->status ? 'Activo' : 'Inactivo') }}
									</td>
									<td><a href="javascript:void(0);" onclick="editarRol({{ $rol }})">Modificar</a></td>
								</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="div-nuevo-rol" style="display: none;">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Agregar nuevo rol</h6>
			</div>
			<div class="card-body">
				<form action="{{ route('roles.nuevo') }}" method="POST" id="form-nuevo-rol">
					{{ csrf_field() }}
					@if($errors->any())
						<script>
							$('#div-nuevo-rol').show();
						</script>
					@endif
					<div class="form-group">
						<label for="nombre">Dale un nombre *</label>
						<input type="text" id="nombre" name="nombre" class="form-control" placeholder="ej. Administración" value="{{ old('nombre') }}">
						@if($errors->has('nombre'))
							<!-- <script>$('#nombre').addClass('is-invalid');</script> -->
							<small style="color: red">Revisa este campo</small>
						@endif
					</div>
					<div class="form-group">
						<label for="select2Multiple">Selecciona las áreas permitidas (<b>del SISTEMA</b>)* <span class="badge badge-pill badge-light bg-dark areas-tooltip" title="Las áreas del sistema son las opciones que aparecen en el menú de la parte izquierda. Son las secciones que el usuario podrá ver o no, según su elección." ><i class="fa fa-question text-white"></i></span></label>
						<select class="select2-multiple form-control" name="areas[]" multiple="multiple" id="select2Multiple">
	                      	<option value="" disabled="">Selecciona los datos</option>
	                      	@foreach($areas as $area)
	                      	<option value="{{ $area->id }}">{{ $area->nombre }}</option>
	                      	@endforeach()
	                    </select>
	                    @if($errors->has('areas'))
							<small style="color: red">Revisa este campo</small>
						@endif
					</div>
					<div class="div-botones float-right">
						<div class="btn-group">
							<button type="button" class="btn btn-secondary" id="boton-cancelar-nuevo-rol">Cancelar</button>
							<button type="submit" class="btn btn-primary">Agregar</button>
						</div>	
					</div>
				</form>
			</div>
			<div class="card-footer text-gray">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="div-modificar-rol" style="display: none;">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Modificar rol</h6>
			</div>
			<div class="card-body">
				<form action="{{ route('roles.modificar') }}" method="POST" id="form-modificar-rol">
					{{ csrf_field() }}
					<input type="hidden" name="rol_id" id="rol_id" value="">
					<div class="form-group">
						<label for="nombre">Nombre *</label>
						<input type="text" id="nombre-mod" name="nombre" class="form-control">
					</div>
					<div class="form-group">
						<label for="select2Multiple">Selecciona las áreas permitidas (<b>del SISTEMA</b>)*</label>
						<select class="select2-multiple form-control" name="areas[]" multiple="multiple" id="select2Multiple-mod">
	                      	<option value="" disabled="">Selecciona los datos</option>
	                      	@foreach($areas as $area)
	                      	<option value="{{ $area->id }}" id="option_{{ $area->id }}">{{ $area->nombre }}</option>
	                      	@endforeach()
	                    </select>
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="status" name="status">
                            <label class="custom-control-label" for="status" id="label-status"></label>
                        </div> 
					</div>
					<div class="div-botones float-right">
						<div class="btn-group">
							<button type="button" class="btn btn-secondary" id="boton-cancelar-modificar-rol">Cancelar</button>
							<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#eliminarRolModal" id="btn-eliminar">Eliminar</button>
							<button type="submit" class="btn btn-primary">Modificar</button>
						</div>	
					</div>
				</form>
			</div>
			<div class="card-footer text-gray">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	@include('administracion.roles.modal')
</div>

<script>

	function cancelarNuevoRol(){
		$('#div-nuevo-rol').hide();
		$('#nombre').val('');
		$('#select2Multiple').select2("val", "");

		$('#div-modificar-rol').hide();
		$('#nombre-mod').val('');
		$('#select2Multiple-mod').select2("val", "");
	}

	function editarRol(rol){
		console.log(rol);
		var areas = [];
		$('#rol_id').val(rol.id);
		$('#rol_id2').val(rol.id);
		$('#div-nuevo-rol').hide();
		(rol.eliminable == 0 ? $('#btn-eliminar').hide() : $('#btn-eliminar').show())
		$('#nombre-mod').val(rol.nombre);
		if(rol.status==1){
			$('#status').attr('checked', 'true');
			$('#label-status').text('Rol Activo');
		}else{
			$('#label-status').text('Rol Inactivo')
		}
		$('#div-modificar-rol').show();

		areas = rol.areas.split(',');

		$("#select2Multiple-mod").select2({
            multiple: true,
        });
        $('#select2Multiple-mod').val(areas).trigger('change');
	}

	$(document).ready(function(){

		$('#admin-general').addClass('active');
		$('#collapseBootstrap').addClass('show');
		$('#admin-item-roles').addClass('active');

		$('#boton-agregar-nuevo-rol').click(function(){
			$('#div-nuevo-rol').show();
			$('#div-modificar-rol').hide();
		});

		$('#boton-cancelar-nuevo-rol').click(function(){
			cancelarNuevoRol();
		});

		$('#boton-cancelar-modificar-rol').click(function(){
			cancelarNuevoRol();
		});

		$('#status').click(function(){
			if ($(this).is(':checked')) $('#label-status').text('Rol Activo');
			else $('#label-status').text('Rol Inactivo');
		})

		$('.select2-multiple').select2();
		$('.select2-container--default').css('width', '100%');

		$('#tabla-roles').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		@if($errors->has('foreign'))
			$('#foreign').show();
		@endif()

		$('.areas-tooltip').tooltip();
	});
</script>

@endsection()

<table>
	<thead>
		<tr>
			<th>Laboratorio</th>
			<th>Maquina</th>
			<th>Usuario</th>
			<th>Tipo incidente</th>
			<th>Comentario</th>
			<th>Prioridad</th>
			<th>Status</th>
			<th>Fecha</th>
		</tr>
	</thead>
	<tbody>
		@foreach($datos as $dato)
			<tr>
				<td>{{$dato->laboratorio}}</td>
				<td>{{$dato->equipo}}</td>
				<td>{{$dato->usuario}}</td>
				<td>{{$dato->incidente}}</td>
				<td>{{$dato->comentario}}</td>
				<td>{{$dato->prioridad}}</td>
				<td>{{$dato->status}}</td>
				<td>{{$dato->fecha}}</td>
			</tr>
		@endforeach()
	</tbody>
</table>

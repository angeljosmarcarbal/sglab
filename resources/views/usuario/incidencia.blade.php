@extends('plantilla.layoutAlumno')
@section('sidebar')
    <!--Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon">
                <img src="{{ asset('img/logo/logoescuela.png') }}">
            </div>
        </a>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Menú de usuario
        </div>
        <hr class="sidebar-divider my-0">
        <li class="nav-item" id="item-perfil">
            <a class="nav-link" href="{{route('incidencias')}}">
                <i class="fas fa-fw fa-user"></i>
                <span>Incidencias</span>
            </a>
        </li>
        <li class="nav-item" id="item-perfil">
            <a class="nav-link" href="{{route('alumnoPerfil')}}">
                <i class="fas fa-fw fa-user"></i>
                <span>Mi perfil</span>
            </a>
        </li>


        <div class="version" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar-->
@endsection()
@section('general-content')
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
            <!-- Container Fluid-->
                <div class="container-fluid" id="container-wrapper">

                    @if(session('uso')==true)
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p class="text-gray-800">Tiempo de uso en este equipo</p>
                                        <h1 class="h4 text-primary">
                                            <span id="date"></span>
                                        </h1>
                                    </div>
                                </div>
                            </div>
                        </div><br>
                    @endif()

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Registrar incidencia</h6>
                                    <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#nuevaIncidencia">Agregar nuevo</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Tabla de incidencias-->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Registros de incidencias en equipos</h1>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h6 class="m-0 font-weight-bold text-primary">Registros hechos</h6>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table" id="tabla-bitacora-incidencias">
                                                <thead>
                                                <th>Laboratorio</th>
                                                <th>Equipo</th>
                                                <th>Comentario</th>
                                                <th>Prioridad</th>
                                                <th>Status</th>
                                                </thead>
                                                <tbody>
                                                @foreach($usoincidentes as $usoincidente)
                                                    <tr>
                                                        <td>{{$usoincidente->laboratorio}}</td>
                                                        <td>{{$usoincidente->numero}}</td>
                                                        <td>{{$usoincidente->detalles}}</td>
                                                        <td>{{$usoincidente->prioridad}}</td>
                                                        <td class="text-info">{{$usoincidente->nombre}}</td>

                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Termina tabla de incidencias-->

                    @include('usuario.usoEquipo')
                    @include('usuario.nuevaIncidencia')

                </div>
                <!---Container Fluid-->

            </div>
        </div>
    </div>

    <script>
        $(function(){

            var b = moment("{{session('usoDate')}}");
            var dateT = $('#date');

            setInterval(function(){
                a = moment();
                var x = a.diff(b, 'miliseconds');
                dateT.text(moment.utc(x).format('HH:mm:ss'));
            }, 1000);

        })
    </script>
@endsection()
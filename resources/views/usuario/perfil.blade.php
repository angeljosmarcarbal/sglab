@extends('plantilla.layoutAlumno')
@section('sidebar')
	<!--Sidebar -->
	<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
		<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
			<div class="sidebar-brand-icon">
				<img src="{{ asset('img/logo/logoescuela.png') }}">
			</div>
			<!-- <div class="sidebar-brand-text mx-3">SgLab</div> -->
		</a>
		<hr class="sidebar-divider">
		<div class="sidebar-heading">
			Menú de usuario
		</div>
		<hr class="sidebar-divider my-0">
		<li class="nav-item" id="item-perfil">
			<a class="nav-link" href="{{route('incidencias')}}">
				<i class="fas fa-fw fa-user"></i>
				<span>Incidencias</span>
			</a>
		</li>
		<li class="nav-item" id="item-perfil">
			<a class="nav-link" href="{{route('alumnoPerfil')}}">
				<i class="fas fa-fw fa-user"></i>
				<span>Mi perfil</span>
			</a>
		</li>


		<div class="version" id="version-ruangadmin"></div>
	</ul>
	<!-- Sidebar-->
@endsection()
@section('general-content')
	<div class="row">
		<div class="col">
			<div class="card mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h6 class="m-0 font-weight-bold text-primary">Perfil</h6>
				</div>
				<div class="card-body">
					<form action="">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" id="nombre" class="form-control" value="{{$usuario->nombre}}">
						</div>
						<div class="form-group">
							<label for="apellido_paterno">Apellido paterno</label>
							<input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" value="{{$usuario->apellido_paterno}}">
						</div>
						<div class="form-group">
							<label for="apellido_materno">Apellido materno</label>
							<input type="text" name="apellido_materno" id="apellido_materno" class="form-control" value="{{$usuario->apellido_materno}}">
						</div>
						<div class="form-group">
							<label for="nombre">Email</label>
							<input type="text" name="email" id="email" class="form-control" value="{{$usuario->email}}">
						</div>
						
						
					</form>
				</div>
			</div>
		</div>

	</div>

	<script>
		$(document).ready(function(){
			$('#item-perfil').addClass('active');
		});
	</script>

@endsection()

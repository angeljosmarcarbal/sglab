<div class="modal fade" id="nuevaIncidencia" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
     aria-hidden="true">
    <form action="{{ route('altaIncidencias') }}" method="POST" id="form-nuevo-usuario">
        {{csrf_field()}}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="exampleModalLabelLogout">Agregar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    @foreach($usoActual as $row)
                                    <div class="form-group">
                                        <label for="matricula">Matricula</label>                                        
                                        <input type="text" class="form-control" name="matricula" id="matricula" value="{{$row->matricula}}" readonly="">
                                    </div>
                                    <div class="form-group">
                                        <label for="matricula">Laboratorio</label>
                                        <input type="text" class="form-control" name="matricula" id="matricula" value="{{$row->nombre}}" readonly="">
                                    </div>
                                    <div class="form-group">
                                        <label for="matricula">Numero de equipo</label>
                                        <input type="text" class="form-control" name="matricula" id="matricula" value="{{$row->numero}} {{$row->marca}} {{$row->modelo}}" readonly="">

                                    </div>
                                    @endforeach
                                </div>
                            </div>                         
                            <div class="form-group">
                                <label for="incidencia">Tipo de problema*</label>
                                <select name="incidencia" id="incidencia" class="form-control">
                                    <option value="">Seleccione un problema</option>
                                    @foreach($tipoIncidencias as $tipoIncidencia)
                                        <option value="{{$tipoIncidencia->id}}">{{$tipoIncidencia->nombre}}: {{$tipoIncidencia->descripcion}}</option>
                                    @endforeach()
                                </select>
                            @if($errors->has('laboratorio'))
                                <!-- <span style="color: red;">{{$errors->first('rol')}}</span> -->
                                    <span style="color: red;">Revisa este dato</span>
                                @endif()
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="detalles">Detalles</label>
                                    <input type="textarea" maxlength="255" class="form-control" name="detalles" id="detalles" placeholder="Describe el problema" required="">
                                    <input type="text" class="form-control" name="uso" id="uso" value="{{$id_uso}}" hidden="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="prioridad">Prioridad*</label>
                                <select class="form-control" name="prioridad" id="prioridades">
                                    <option value="">Selecciona una prioridad</option>
                                    @foreach($prioridades as $prioridad)
                                        <option value="{{$prioridad->id}}">{{$prioridad->nombre}}</option>
                                    @endforeach()
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


<div class="modal fade" id="usoEquipo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
     aria-hidden="true">
    <form action="{{ route('altaIncidencias') }}" method="POST" id="form-nuevo-usuario">
        {{csrf_field()}}
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-primary" id="exampleModalLabelLogout">Agregar usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="matricula">Matricula*</label>
                                        <input type="text" class="form-control" name="matricula" id="matricula" value="{{Auth::user()->matricula}}" readonly="">
                                        <input type="text" class="form-control" name="id_usuario" id="id_usuario" value="{{Auth::user()->id}}" hidden="">

                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label for="laboratorio">Laboratorio*</label>
                                <select name="laboratorio" id="laboratorio" class="form-control">
                                    <option value="">Seleccione un Laboratorio</option>
                                    @foreach($laboratorios as $laboratorio)
                                        <option value="{{$laboratorio->id}}">{{$laboratorio->nombre}}</option>
                                    @endforeach()
                                </select>
                            @if($errors->has('laboratorio'))
                                <!-- <span style="color: red;">{{$errors->first('rol')}}</span> -->
                                    <span style="color: red;">Revisa este dato</span>
                                @endif()
                            </div>
                            <div class="form-group">
                                <label for="numero">Numero de maquina</label>
                                <select name="numero" id="numero" class="form-control">
                                    <option value="">Seleccione un numero de maquina</option>
                                    @foreach($laboratorio->iLaboratorios_all as $computadora)
                                        <option value="{{$computadora->id}}">{{$computadora->marca}} {{$computadora->numero}}</option>
                                    @endforeach()
                                </select>
                            @if($errors->has('numero'))
                                <!-- <span style="color: red;">{{$errors->first('rol')}}</span> -->
                                    <span style="color: red;">Revisa este dato</span>
                                @endif()
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-primary">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


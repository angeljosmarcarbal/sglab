
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('plantilla.header')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
</head>
<body id="page-top">

<div id="wrapper">

    <!--Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
            <div class="sidebar-brand-icon">
                <img src="{{ asset('img/logo/logoescuela.png') }}">
            </div>
            <!-- <div class="sidebar-brand-text mx-3">SgLab</div> -->
        </a>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            Opciones
        </div>
        <li class="nav-item" id="item-perfil">
            <a class="nav-link" href="{{route('alumnoPerfil')}}">
                <i class="fas fa-fw fa-user"></i>
                <span>Mi perfil</span>
            </a>
        </li>
        <hr class="sidebar-divider">

        <div class="version" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar-->
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
        @include('plantilla.navbarAlumno')
        <!-- Container Fluid-->
            <div class="container-fluid" id="container-wrapper">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0 text-gray-800">Bienvenido {{Auth::user()->nombre}} {{Auth::user()->apellido_paterno}} {{Auth::user()->apellido_materno}}</h1>
                </div>

                @if(session('uso') == false)

                <div class="row">
                    <div class="col-lg-12">
                            <form action="{{ route('altaUso') }}" method="POST" id="form-nuevo-usuario">
                                {{csrf_field()}}
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title text-primary" id="exampleModalLabelLogout">Registra tu equipo</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="form-group">
                                                                <label for="matricula">Matricula*</label>
                                                                <input type="text" class="form-control" name="matricula" id="matricula" value="{{Auth::user()->matricula}}" readonly="">
                                                                <input type="text" class="form-control" name="id_usuario" id="id_usuario" value="{{Auth::user()->id}}" hidden="">

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="laboratorio">Laboratorio*</label>
                                                        <select name="laboratorio" id="laboratorio" class="form-control">
                                                            <option value="">Seleccione un Laboratorio</option>
                                                            @foreach($laboratorios as $laboratorio)
                                                                <option value="{{$laboratorio->id}}">{{$laboratorio->nombre}}</option>
                                                            @endforeach()
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="numero">Numero de maquina</label>
                                                        <select name="numero" id="numero" class="form-control">
                                                            
                                                            
                                                        </select>
                                                    
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-primary">Agregar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>

                @else

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p class="text-gray-800">Tiempo de uso en este equipo</p>
                                    <h1 class="h4 text-primary">
                                            <span id="date"></span>
                                        </h1>
                                </div>
                            </div>
                        </div>
                    </div>

                @endif()

                @include('usuario.usoEquipo')
            </div>
            <!---Container Fluid-->
        </div>
        <script src="{{ asset('js/usuario/edit.js') }}"></script>
        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
	            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - desarrollado por
	              <b><a href="https://indrijunanda.gitlab.io/" target="_blank">UTSEM</a></b>
	            </span>
                </div>
            </div>
        </footer>
        <!-- Footer -->
    </div>

</div>


@include('plantilla.footer')

 <script>
        $(function(){

            var b = moment("{{session('usoDate')}}");
            var dateT = $('#date');

            setInterval(function(){
                a = moment();
                var x = a.diff(b, 'miliseconds');
                dateT.text(moment.utc(x).format('HH:mm:ss'));
            }, 1000);

        })
    </script>

</body>
</html>
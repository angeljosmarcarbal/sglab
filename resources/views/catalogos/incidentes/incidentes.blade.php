@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tipos de Incidencias</h1>
</div>
<div class="alert alert-danger" id="foreign" style="display: none;">No se puede eliminar. Hay registros en bitácoras con este tipo de incidencia.</div>

<div class="row">
	<div class="col-lg-8">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Incidentes agregados</h6>
				<button class="btn btn-default btn-sm" id="btn-agregar-nuevo">Agregar nuevo</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<style>
							#tabla-laboratorios_info{
								margin-top: 50px!important;
							}
						</style>
						<table class="table" id="tabla-incidentes">
							<thead>
								<th>ID</th>
								<th>Tipo</th>
								<th>Nombre</th>
								<th>Descripción</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($incidentes as $incidente)
									<tr>
										<td>{{$incidente->id}}</td>
										<td>
											@if($incidente->tipo ==1)
												Hardware
											@else()
												Software
											@endif()
										</td>
										<td>{{$incidente->nombre}}</td>
										<td>{{$incidente->descripcion}}</td>
										<td>
											@if($incidente->status ==1)
												Activo
											@else
												Inactivo
											@endif()
										</td>
										<td>
											<a href="javascript:void(0);" onclick="editarIncidente({{$incidente}})">Modificar</a>
										</td>
									</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="nuevo-incidente" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Nuevo tipo de incidente</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('incidentes.nuevo')}}" method="POST" id="form-nuevo-incidente">
							{{csrf_field()}}
							<div class="form-group">
								<label for="tipo">Tipo*</label>
								<select class="select form-control" id="tipo_nuevo" name="tipo_nuevo">
									<option value="">Elije una opción</option>
									<option value="1">Hardware</option>
									<option value="2">Software</option>
								</select>
								<script>
									$('#tipo_nuevo').val({{old('tipo_nuevo')}});
								</script>
								@if($errors->has('tipo_nuevo'))
									<span style="color: red;">Revisa ese dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Nombre*</label>
								<input type="text" class="form-control" name="nombre_nuevo" id="nombre_nuevo" placeholder="ej. Falta de programa" value="{{old('nombre_nuevo')}}">
								@if($errors->has('nombre_nuevo'))
									<span style="color: red;">Revisa ese dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="descripcion_nuevo">Descripción*</label>
								<input type="text" class="form-control" name="descripcion_nuevo" id="descripcion_nuevo" value="{{old('descripcion_nuevo')}}" placeholder="Ej. Software no instalado en el equipo">
								@if($errors->has('descripcion_nuevo'))
									<span style="color: red;">Revisa ese dato</span>
								@endif()
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-nuevo">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	@if($errors->has('tipo_nuevo') || $errors->has('nombre_nuevo') || $errors->has('descripcion_nuevo'))
		<script>
			$('#nuevo-incidente').show()
		</script>
	@endif()

	<div class="col-lg-4" id="editar-incidente" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Modificar incidente</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('incidentes.editar')}}" method="POST" id="form-editar-incidente">
							{{csrf_field()}}
							<div class="form-group">
								<label for="tipo">Tipo*</label>
								<select class="select form-control" id="tipo_editar" name="tipo_editar">
									<option value="">Elije una opción</option>
									<option value="1">Hardware</option>
									<option value="2">Software</option>
								</select>
								<script>
									$('#tipo_editar').val({{old('tipo_editar')}});
								</script>
								@if($errors->has('tipo_editar'))
									<span style="color: red;">Revisa ese dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Nombre*</label>
								<input type="hidden" name="incidente_id" id="incidente_id" value="">
								<input type="text" class="form-control" name="nombre_editar" id="nombre_editar" placeholder="ej. Falta de programa" value="{{old('nombre_editar')}}">
								@if($errors->has('nombre_editar'))
									<span style="color: red;">Revisa ese dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="descripcion_editar">Descripción*</label>
								<input type="text" class="form-control" name="descripcion_editar" id="descripcion_editar" placeholder="ej. Falta de programa" value="{{old('descripcion_editar')}}">
								@if($errors->has('descripcion_editar'))
									<span style="color: red;">Revisa ese dato</span>
								@endif()
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
		                            <input type="checkbox" class="custom-control-input" id="status" name="status">
		                            <label class="custom-control-label" for="status" id="label-status">Activo / Inactivo</label>
		                        </div> 
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-modificar">Cancelar</button>
								<button type="button" class="btn btn-danger" id="btn-cancelar-eliminar" data-toggle="modal" data-target="#eliminarIncidenteModal">Eliminar</button>
								<button type="submit" class="btn btn-primary">Modificar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>
</div>

@if($errors->has('tipo_editar') || $errors->has('nombre_editar') || $errors->has('descripcion_editar'))
		<script>
			$('#editar-incidente').show()
		</script>
	@endif()

@include('catalogos.incidentes.modal')


<script>
	function editarIncidente(incidente){
		$('#nuevo-incidente').hide();
		$('#editar-incidente').show();
		$('#status').removeAttr('checked');
		$('#label-status').text('');
		$('#tipo_editar').val(incidente.tipo);
		$('#nombre_editar').val(incidente.nombre);
		$('#descripcion_editar').val(incidente.descripcion);
		$('#incidente_id').val(incidente.id);
		$('#incidente_id2').val(incidente.id);
		if(incidente.status ==1){
			$('#status').attr('checked', true);
			$('#label-status').text('Incidente Activo');
		}else{
			$('#status').attr('checked', false);
			$('#label-status').text('Incidente Inactivo');
		}
	}
	$(document).ready(function(){
		$('#catalogos-general').addClass('active');
		$('#collapseForm').addClass('show');
		$('#catalogos-item-incidentes').addClass('active');

		$('#tabla-incidentes').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		$('#btn-agregar-nuevo').click(function(){
			$('#editar-incidente').hide();
			$('#nuevo-incidente').show();
		});

		$('#btn-cancelar-nuevo').click(function(){
			$('#nuevo-incidente').hide();
		});

		$('#btn-cancelar-modificar').click(function(){
			$('#editar-incidente').hide();
		});
	});

	@if($errors->has('foreign'))
			$('#foreign').show();
		@endif()
</script>

@endsection()


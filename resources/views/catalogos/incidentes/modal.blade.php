<div class="modal fade" id="eliminarIncidenteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <form action="{{ route('incidentes.eliminar') }}" method="POST" id="form-eliminar-incidente">
                    {{csrf_field()}}
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>¿Está seguro de querer eliminar este tipo de incidente?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input type="hidden" name="incidente_id" id="incidente_id2" value="">
                    <button type="submit" class="btn btn-primary">Eliminar</button>
                </div>
                </form>
              </div>
            </div>
          </div>
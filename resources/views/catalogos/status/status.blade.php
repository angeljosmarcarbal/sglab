@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tipos de status</h1>
</div>

<div class="row">
	<div class="col-lg-8">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Status agregados</h6>
				<button class="btn btn-default btn-sm" id="btn-agregar-nuevo">Agregar nuevo</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<style>
							#tabla-laboratorios_info{
								margin-top: 50px!important;
							}
						</style>
						<table class="table" id="tabla-status">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<th>Atendido por</th>
								<th>Descripción</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($status as $item)
									<tr>
										<td>{{$item->id}}</td>
										<td>{{$item->nombre}}</td>
										<td>{{$item->atendido_por}}</td>
										<td>{{$item->descripcion}}</td>
										<td>
											@if($item->status ==1)
												Activo
											@else
												Inactivo
											@endif()
										</td>
										<td>
											<a href="javascript:void(0);" onclick="editarStatus({{$item}})">Modificar</a>
										</td>
									</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="nuevo-status" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Nuevo status</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('status.nuevo')}}" method="POST" id="form-nuevo-status">
							{{csrf_field()}}
							<div class="form-group">
								<label for="nombre">Nombre*</label>
								<input type="text" class="form-control" name="nombre_nuevo" id="nombre_nuevo" placeholder="ej. Recibido" value="{{old('nombre_nuevo')}}">
								@if($errors->has('nombre_nuevo'))
									<script>
										$('#nuevo-status').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="atendido_por_nuevo">Atendido por*</label>
								<input type="text" class="form-control" name="atendido_por_nuevo" id="atendido_por_nuevo" placeholder="ej. Nombre" value="{{old('atendido_por_nuevo')}}">
								@if($errors->has('atendido_por_nuevo'))
									<script>
										$('#nuevo-status').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="descripcion_nuevo">Descripción*</label>
								<input type="text" class="form-control" name="descripcion_nuevo" id="descripcion_nuevo" placeholder="ej. Descripción" value="{{old('descripcion_nuevo')}}">
								@if($errors->has('descripcion_nuevo'))
									<script>
										$('#nuevo-status').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-nuevo">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="editar-status" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Modificar status</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('status.editar')}}" method="POST" id="form-editar-status">
							{{csrf_field()}}
							<input type="hidden" id="status_id" name="status_id" value="{{old('status_id')}}">
							<div class="form-group">
								<label for="nombre">Nombre*</label>
								<input type="text" class="form-control" name="nombre_editar" id="nombre_editar" placeholder="ej. Recibido" value="{{old('nombre_editar')}}">
								@if($errors->has('nombre_editar'))
									<script>
										$('#editar-status').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="atendido_por_editar">Atendido por*</label>
								<input type="text" class="form-control" name="atendido_por_editar" id="atendido_por_editar" placeholder="ej. Recibido" value="{{old('atendido_por_editar')}}">
								@if($errors->has('atendido_por_editar'))
									<script>
										$('#editar-status').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="descripcion_editar">Descripción*</label>
								<input type="text" class="form-control" name="descripcion_editar" id="descripcion_editar" placeholder="ej. Recibido" value="{{old('descripcion_editar')}}">
								@if($errors->has('descripcion_editar'))
									<script>
										$('#editar-status').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
		                            <input type="checkbox" class="custom-control-input" id="status" name="status">
		                            <label class="custom-control-label" for="status" id="label-status">Activo / Inactivo</label>
		                        </div> 
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-modificar">Cancelar</button>
								<button type="button" class="btn btn-danger" id="btn-cancelar-eliminar" data-toggle="modal" data-target="#eliminarStatusModal">Eliminar</button>
								<button type="submit" class="btn btn-primary">Modificar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>
</div>

@include('catalogos.status.modal')


<script>
	function editarStatus(status){
		$('#nuevo-status').hide();
		$('#editar-status').show();
		$('#status').removeAttr('checked');
		$('#label-status').text('');
		$('#status_id').val(status.id);
		$('#status_id2').val(status.id);
		$('#nombre_editar').val(status.nombre);
		$('#atendido_por_editar').val(status.atendido_por);
		$('#descripcion_editar').val(status.descripcion);
		if(status.status ==1){
			$('#status').attr('checked', true);
			$('#label-status').text('Status Activo');
		}else{
			$('#status').attr('checked', false);
			$('#label-status').text('Status Inactivo');	
		}
	}
	$(document).ready(function(){
		$('#catalogos-general').addClass('active');
		$('#collapseForm').addClass('show');
		$('#catalogos-item-status').addClass('active');

		$('#tabla-status').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		$('#btn-agregar-nuevo').click(function(){
			$('#editar-status').hide();
			$('#nuevo-status').show();
		});

		$('#btn-cancelar-nuevo').click(function(){
			$('#nuevo-status').hide();
		});

		$('#btn-cancelar-modificar').click(function(){
			$('#editar-status').hide();
		});

		$('#status').click(function(){
			if ($(this).is(':checked')) $('#label-status').text('Status Activo');
			else $('#label-status').text('Status Inactivo');
		});
	});
</script>

@endsection()


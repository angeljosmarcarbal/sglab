@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Equipos agregados al Laboratorio @foreach($laboratorios as $laboratorio) {{$laboratorio->nombre}} @endforeach()  </h1>
</div>
<div class="alert alert-danger" id="foreign" style="display: none;">No se puede eliminar. Hay registros en bitácoras con este equipo.</div>

<div class="row">
	<div class="col-lg-8">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Equipos agregados</h6>
				<button class="btn btn-default btn-sm" id="btn-agregar-nuevo">Agregar nuevo</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<style>
							#tabla-laboratorios_info{
								margin-top: 50px!important;
							}
						</style>
						<table class="table" id="tabla-laboratorios">
							<thead>
								<th>ID</th>
								<th>Marca</th>
								<th>Modelo</th>
								<th>Número</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($computadoras as $computadora)
									<tr>
										<td>{{$computadora->id}}</td>
										<td>{{$computadora->marca}}</td>
										<td>{{$computadora->modelo}}</td>
										<td>{{$computadora->numero}}</td>
										<td>
											@if($computadora->status == 1)
												Activa
											@else()
												Inactiva
											@endif()
										</td>
										<td>
											<a href="javascript:void(0);" onclick="editarComputadora({{$computadora}})">Modificar</a>
										</td>
									</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="nuevo-laboratorio" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Nuevo equipo</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('computadoras.nuevo')}}" method="POST" id="form-nuevo-laboratorio">
							{{csrf_field()}}
							<div class="form-group">
								<input type="hidden" name="laboratorio_id" id="laboratorio_id" value="{{$idLaboratorio}}">
								<label for="nombre">Numero*</label>
								<input type="text" class="form-control" name="numero_nuevo" id="numero_nuevo" placeholder="ej. 10" value="{{old('numero_nuevo')}}">
								@if($errors->has('numero_nuevo'))
									<script>
										$('#nuevo-laboratorio').show()
									</script>
									<span style="color:red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Marca*</label>
								<input type="text" class="form-control" name="marca_nuevo" id="marca_nuevo" placeholder="ej. Acer" value="{{old('marca_nuevo')}}">
								@if($errors->has('marca_nuevo'))
									<script>
										$('#nuevo-laboratorio').show()
									</script>
									<span style="color:red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Modelo*</label>
								<input type="text" class="form-control" name="modelo_nuevo" id="modelo_nuevo" placeholder="ej. Acer" value="{{old('modelo_nuevo')}}">
								@if($errors->has('modelo_nuevo'))
									<script>
										$('#nuevo-laboratorio').show()
									</script>
									<span style="color:red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-nuevo">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="editar-laboratorio" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Modificar equipo</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('computadoras.editar')}}" method="POST" id="form-nuevo-laboratorio">
							{{csrf_field()}}
							<input type="hidden" name="laboratorio_id" id="laboratorio_id" value="{{$idLaboratorio}}">
							<input type="hidden" name="computadora_id" id="computadora_id" value="{{$idLaboratorio}}">
							<div class="form-group">
								<label for="nombre">Numero*</label>
								<input type="text" class="form-control" name="numero_editar" id="numero_editar" placeholder="ej. 10" value="{{old('numero_editar')}}">
								@if($errors->has('numero_editar'))
									<script>
										$('#editar-laboratorio').show()
									</script>
									<span style="color:red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Marca*</label>
								<input type="text" class="form-control" name="marca_editar" id="marca_editar" placeholder="ej. Acer" value="{{old('marca_editar')}}">
								@if($errors->has('marca_editar'))
									<script>
										$('#editar-laboratorio').show()
									</script>
									<span style="color:red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Modelo*</label>
								<input type="text" class="form-control" name="modelo_editar" id="modelo_editar" placeholder="ej. Acer" value="{{old('modelo_editar')}}">
								@if($errors->has('modelo_editar'))
									<script>
										$('#editar-laboratorio').show()
									</script>
									<span style="color:red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
		                            <input type="checkbox" class="custom-control-input" id="status" name="status">
		                            <label class="custom-control-label" for="status" id="label-status"></label>
		                        </div> 
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-modificar">Cancelar</button>
								<button type="button" class="btn btn-danger" id="btn-cancelar-eliminar" data-toggle="modal" data-target="#eliminarComputadoraModal">Eliminar</button>
								<button type="submit" class="btn btn-primary">Modificar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>
</div>

@include('catalogos.computadoras.modal')

<script>
	function editarComputadora(computadora){
		console.log(computadora);
		$('#nuevo-laboratorio').hide();
		$('#editar-laboratorio').show();
		$('#computadora_id').val(computadora.id);
		$('#computadora_id2').val(computadora.id);
		$('#numero_editar').val(computadora.numero);
		$('#marca_editar').val(computadora.marca);
		$('#modelo_editar').val(computadora.modelo);
		$('#status').removeAttr('checked');
		$('#label-status').text('');
		if(computadora.status==1){
			$('#status').attr('checked', true);
			$('#label-status').text('Computadora Activa');
		}else{
			$('#status').removeAttr('checked');
			$('#label-status').text('Computadora Inactiva');
		}
	}
	$(document).ready(function(){
		$('#catalogos-general').addClass('active');
		$('#collapseForm').addClass('show');
		$('#catalogos-item-laboratorios').addClass('active');

		$('#tabla-laboratorios').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		$('#btn-agregar-nuevo').click(function(){
			$('#editar-laboratorio').hide();
			$('#nuevo-laboratorio').show();
		});

		$('#btn-cancelar-nuevo').click(function(){
			$('#nuevo-laboratorio').hide();
		});

		$('#btn-cancelar-modificar').click(function(){
			$('#editar-laboratorio').hide();
		});

		$('#status').click(function(){
			if ($(this).is(':checked')) $('#label-status').text('Compuradora Activa');
			else $('#label-status').text('Compuradora Inactiva');
		});
	});

	@if($errors->has('foreign'))
			$('#foreign').show();
		@endif()
</script>

@endsection()


@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tipos de prioridades</h1>
</div>

<div class="row">
	<div class="col-lg-8">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Prioridades agregadas</h6>
				<button class="btn btn-default btn-sm" id="btn-agregar-nuevo">Agregar nueva</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<style>
							#tabla-laboratorios_info{
								margin-top: 50px!important;
							}
						</style>
						<table class="table" id="tabla-prioridades">
							<thead>
								<th>ID</th>
								<th>Tipo</th>
								<th>Nombre</th>
								<th>Descripción</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($prioridades as $prioridad)
									<tr>
										<td>{{$prioridad->id}}</td>
										<td>
											@if($prioridad->tipo==1)
												Muy alta
											@elseif($prioridad->tipo==2)
												Alta
											@elseif($prioridad->tipo==3)
												Media
											@else
												Baja
											@endif()
										</td>
										<td>{{$prioridad->nombre}}</td>
										<td>{{$prioridad->descripcion}}</td>
										<td>
											@if($prioridad->status == 1)
												Activa
											@else
												Inactiva
											@endif()
										</td>
										<td>
											<a href="javascript:void(0);" onclick="editarPrioridad({{$prioridad}})">Modificar</a>
										</td>
									</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="nueva-prioridad" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Nueva prioridad</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">						
						<form action="{{ route('prioridades.nuevo') }}" method="POST" id="form-nuevo-prioridad">
							{{csrf_field()}}
							<div class="form-group">
								<label for="tipo">Tipo*</label>
								<select class="select form-control" name="tipo_nuevo" id="tipo_nuevo">
									<option value="">Elije una opción</option>
									<option value="1">Muy alta</option>
									<option value="2">Alta</option>
									<option value="3">Media</option>
									<option value="4">Baja</option>
								</select>
								<script>
									$('#tipo_nuevo').val({{old('tipo_nuevo')}});
								</script>
								@if($errors->has('tipo_nuevo'))
								    <span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<label for="nombre">Nombre*</label>
								<input type="text" class="form-control" name="nombre_nuevo" id="nombre" placeholder="ej. Falta de programa" value="{{old('nombre_nuevo')}}">
								@if($errors->has('nombre_nuevo'))
									<small style="color: red">Revisa este campo</small>
								@endif()
							</div>
							<div class="form-group">
								<label for="descripcion_nuevo">Descripción*</label>
								<input type="text" class="form-control" name="descripcion_nuevo" id="descripcion_nuevo" placeholder="ej. Falta de programa" value="{{old('descripcion_nuevo')}}">
								@if($errors->has('descripcion_nuevo'))
									<small style="color: red">Revisa este campo</small>
								@endif()
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-nuevo">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	@if($errors->has('tipo_nuevo') || $errors->has('nombre_nuevo') || $errors->has('descripcion_nuevo'))
		<script type="text/javascript">
			$('#nueva-prioridad').show();
		</script>
	@endif()

	<div class="col-lg-4" id="editar-prioridad" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Modificar prioridad</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{ route('prioridades.modificar') }}" method="POST" id="form-editar-prioridad">
							{{csrf_field()}}
							<div class="form-group">
								<label for="tipo">Tipo*</label>
								<select class="select form-control" name="tipo_editar" id="tipo_editar">
									<option value="">Elije una opción</option>
									<option value="1">Muy alta</option>
									<option value="2">Alta</option>
									<option value="3">Media</option>
									<option value="4">Baja</option>
								</select>
								<script>
									$('#tipo_editar').val({{old('tipo_editar')}});
								</script>
								@if($errors->has('tipo_editar'))
									<script>
										$('#editar-prioridad').show();
									</script>
								    <span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<input type="hidden" name="prioridad_id" id="prioridad_id" value="{{old('prioridad_id')}}">
								<label for="nombre">Nombre*</label>
								<input type="text" class="form-control" name="nombre_editar" id="nombre_editar" placeholder="ej. Falta de programa" value="{{old('nombre_editar')}}">
								@if($errors->has('nombre_editar'))
									<script>
										$('#editar-prioridad').show();
									</script>
									<small style="color: red">Revisa este campo</small>
								@endif()
							</div>
							<div class="form-group">
								<label for="descripcion">Descripción*</label>
								<input type="text" class="form-control" name="descripcion_editar" id="descripcion_editar" placeholder="ej. Falta de programa" value="{{old('descripcion_editar')}}">
								@if($errors->has('descripcion_editar'))
									<script>
										$('#editar-prioridad').show();
									</script>
									<small style="color: red">Revisa este campo</small>
								@endif()
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
		                            <input type="checkbox" class="custom-control-input" id="status" name="status">
		                            <label class="custom-control-label" for="status" id="label-status"></label>
		                        </div> 
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-modificar">Cancelar</button>
								<button type="button" class="btn btn-danger" id="btn-cancelar-eliminar" data-toggle="modal" data-target="#eliminarPrioridadModal">Eliminar</button>
								<button type="submit" class="btn btn-primary">Modificar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>
</div>

@include('catalogos.prioridades.modal')


<script>
	function editarPrioridad(prioridad){
		$('#status').removeAttr('checked');
		$('#label-status').text('');
		$('#nueva-prioridad').hide();
		$('#editar-prioridad').show();
		$('#prioridad_id').val(prioridad.id);
		$('#prioridad_id2').val(prioridad.id);
		$('#tipo_editar').val(prioridad.tipo);
		$('#nombre_editar').val(prioridad.nombre);
		$('#descripcion_editar').val(prioridad.descripcion);
		if(prioridad.status==1){
			$('#status').attr('checked', 'true');
			$('#label-status').text('Prioridad Activa');
		}else{
			$('#label-status').text('Prioridad Inactiva')
		}
	}
	$(document).ready(function(){
		$('#catalogos-general').addClass('active');
		$('#collapseForm').addClass('show');
		$('#catalogos-item-prioridades').addClass('active');

		$('#tabla-prioridades').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		$('#btn-agregar-nuevo').click(function(){
			$('#editar-prioridad').hide();
			$('#nueva-prioridad').show();
		});

		$('#btn-cancelar-nuevo').click(function(){
			$('#nueva-prioridad').hide();
		});

		$('#btn-cancelar-modificar').click(function(){
			$('#editar-prioridad').hide();
			$('#nombre_editar').val('');
		});

		$('#status').click(function(){
			if ($(this).is(':checked')) $('#label-status').text('Prioridad Activa');
			else $('#label-status').text('Prioridad Inactiva');
		});
	});
</script>

@endsection()


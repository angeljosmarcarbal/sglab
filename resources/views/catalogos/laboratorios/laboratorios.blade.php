@extends('plantilla.layout')

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Catalogo de laboratorios</h1>
</div>
<div class="alert alert-danger" id="foreign" style="display: none;">No se puede eliminar. Hay registros en bitácoras con este laboratorio.</div>

<div class="row">
	<div class="col-lg-8">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Laboratorios agregados</h6>
				<button class="btn btn-default btn-sm" id="btn-agregar-nuevo">Agregar nuevo</button>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<style>
							#tabla-laboratorios_info{
								margin-top: 50px!important;
							}
						</style>
						<table class="table" id="tabla-laboratorios">
							<thead>
								<th>ID</th>
								<th>Nombre</th>
								<th>Status</th>
								<th>Acciones</th>
							</thead>
							<tbody>
								@foreach($laboratorios as $laboratorio)
									<tr>
										<td>{{$laboratorio->id}}</td>
										<td>{{$laboratorio->nombre}}</td>
										<td>
											@if($laboratorio->status ==1)
												Activo
											@else()
												Inactivo
											@endif()
										</td>
										<td>
											<a class="btn btn-sm btn-primary" href="javascript:void(0);" onclick="editarLaboratorio({{$laboratorio}})">Modificar</a>
											<a class="btn btn-sm btn-info" href="{{route('computadoras.show', $laboratorio->id)}}">Ver equipos</a>
										</td>
									</tr>
								@endforeach()
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="nuevo-laboratorio" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Nuevo laboratorio</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('laboratorios.nuevo')}}" method="POST" id="form-nuevo-laboratorio">
							{{csrf_field()}}
							<div class="form-group">
								<label for="nombre">Dale un nombre*</label>
								<input type="text" class="form-control" name="nombre_nuevo" id="nombre_nuevo" placeholder="ej. Laboratorio CC1">
								@if($errors->has('nombre_nuevo'))
									<script>
										$('#nuevo-laboratorio').show();
									</script>
									<span style="color:red;">Revisa este campo</span>
								@endif()
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-nuevo">Cancelar</button>
								<button type="submit" class="btn btn-primary">Agregar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>

	<div class="col-lg-4" id="editar-laboratorio" style="display: none;">
		<div class="card mb4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Modificar laboratorio</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<form action="{{route('laboratorios.editar')}}" method="POST" id="form-nuevo-laboratorio">
							{{csrf_field()}}
							<div class="form-group">
								<label for="nombre">Nombre*</label>
								<input type="hidden" name="laboratorio_id" id="laboratorio_id" value="">
								<input type="text" class="form-control" name="nombre_editar" id="nombre_editar" placeholder="ej. Laboratorio CC1">
								@if($errors->has('nombre_editar'))
									<script>
										$('#editar-laboratorio').show();
									</script>
									<span style="color: red;">Revisa este dato</span>
								@endif()
							</div>
							<div class="form-group">
								<div class="custom-control custom-checkbox">
		                            <input type="checkbox" class="custom-control-input" id="status" name="status">
		                            <label class="custom-control-label" for="status" id="label-status"></label>
		                        </div> 
							</div>
							<div class="btn-group float-right">
								<button type="button" class="btn btn-secondary" id="btn-cancelar-modificar">Cancelar</button>
								<button type="button" class="btn btn-danger" id="btn-cancelar-eliminar" data-toggle="modal" data-target="#eliminarLaboratorioModal">Eliminar</button>
								<button type="submit" class="btn btn-primary">Modificar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="card-footer">
				<small>Los campos con * son obligatorios</small>
			</div>
		</div>
	</div>
</div>

@include('catalogos.laboratorios.modal')


<script>
	function editarLaboratorio(laboratorio){
		$('#nuevo-laboratorio').hide();
		$('#editar-laboratorio').show();
		$('#status').removeAttr('checked');
		$('#label-status').text('');
		$('#nombre_editar').val(laboratorio.nombre);
		$('#laboratorio_id').val(laboratorio.id);
		$('#laboratorio_id2').val(laboratorio.id);
		if(laboratorio.status==1){
			$('#status').attr('checked', 'true');
			$('#label-status').text('Laboratorio Activo');
		}else{
			$('#label-status').text('Laboratorio Activo');
		}
	}
	$(document).ready(function(){
		$('#catalogos-general').addClass('active');
		$('#collapseForm').addClass('show');
		$('#catalogos-item-laboratorios').addClass('active');

		$('#tabla-laboratorios').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			}
		});

		$('#btn-agregar-nuevo').click(function(){
			$('#editar-laboratorio').hide();
			$('#nuevo-laboratorio').show();
		});

		$('#btn-cancelar-nuevo').click(function(){
			$('#nuevo-laboratorio').hide();
		});

		$('#btn-cancelar-modificar').click(function(){
			$('#editar-laboratorio').hide();
		});

		$('#status').click(function(){
			if ($(this).is(':checked')) $('#label-status').text('Laboratorio Activo');
			else $('#label-status').text('Laboratorio Inactivo');
		});
	});

	@if($errors->has('foreign'))
			$('#foreign').show();
		@endif()
</script>

@endsection()


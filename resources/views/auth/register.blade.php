<!DOCTYPE html>
<html lang="es-Mx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/logo/logoescuela.png') }}">
    <title>SgLab</title>

    <!-- Styles -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/ruang-admin.min.css') }}" rel="stylesheet">
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <style>
      .has-error{
        border: 1px solid red;
      }
    </style>
</head>
  <body>

    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="row">
            <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12 m-auto">
              <div class="card my-3">
                <div class="card-header">
                  <h6 class="h5 text-primary">Registro de nuevo docente</h6>
                  @if($errors->has('saveError'))
                    <div class="alert alert-danger" id="alert">{{$errors->first('saveError')}}</div>
                    <script>setTimeout(function(){ $('#alert').hide() }, 6000)</script>
                  @endif()
                </div>
                <div class="card-body">
                  <form action="{{ route('register') }}" method="POST" id="register-form">
                    {{ csrf_field() }}

                    <div class="row">
                      <div class="col-lg-6">

                        <div class="form-group">
                          <label for="nombre" class="col-md-12 control-label">Nombre *</label>
                            <div class="col-md-12">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' has-error' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>
                                @if ($errors->has('nombre'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="apellido_paterno" class="col-md-12 control-label">Apellido Paterno *</label>
                            <div class="col-md-12">
                                <input id="apellido_paterno" type="text" class="form-control{{ $errors->has('apellido_paterno') ? ' has-error' : '' }}" name="apellido_paterno" value="{{ old('apellido_paterno') }}" required autofocus>
                                @if ($errors->has('apellido_paterno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('apellido_paterno') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="apellido_materno" class="col-md-12 control-label">Apellido Materno *</label>
                            <div class="col-md-12">
                                <input id="apellido_materno" type="text" class="form-control{{ $errors->has('apellido_materno') ? ' has-error' : '' }}" name="apellido_materno" value="{{ old('apellido_materno') }}" required autofocus>
                                @if ($errors->has('apellido_materno'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('apellido_materno') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="telefono" class="col-md-12 control-label">Teléfono *</label>
                            <div class="col-md-12">
                                <input id="telefono" type="text" class="form-control{{ $errors->has('telefono') ? ' has-error' : '' }}" name="telefono" value="{{ old('telefono') }}" required autofocus>
                                @if ($errors->has('telefono'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('telefono') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                      </div>
                      <div class="col-lg-6">

                        <div class="form-group">
                            <label for="id_docente" class="col-md-12 control-label">ID de docente *</label>
                            <div class="col-md-12">
                                <input id="id_docente" type="text" class="form-control{{ $errors->has('id_docente') ? ' has-error' : '' }}" name="id_docente" value="{{ old('id_docente') }}" required autofocus>
                                @if ($errors->has('id_docente'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('id_docente') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="correo" class="col-md-12 control-label">Correo *</label>
                            <div class="col-md-12">
                                <input id="correo" type="text" class="form-control{{ $errors->has('correo') ? ' has-error' : '' }}" name="correo" value="{{ old('correo') }}" required autofocus>
                                @if ($errors->has('correo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('correo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-12 control-label">Password *</label>
                            <div class="col-md-12">
                                <input id="password" type="text" class="form-control{{ $errors->has('password') ? ' has-error' : '' }}" name="password" value="{{ old('password') }}" required autofocus>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label for="confirmar_password" class="col-md-12 control-label">Confirmar Password *</label>
                            <div class="col-md-12">
                                <input id="confirmar_password" type="text" class="form-control{{ $errors->has('confirmar_password') ? ' has-error' : '' }}" name="confirmar_password" value="{{ old('confirmar_password') }}" required autofocus>
                                @if ($errors->has('confirmar_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('confirmar_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div> -->
                        
                      </div>
                    </div>
                    <br>
                    <input type="submit" class="btn btn-primary btn-md btn-block" value="Registrarme">
                  </form>
                </div>
                <div class="card-footer text-center">
                  <hr>
                  <a href="{{ route('login') }}" class="text primary">Ya tengo una cuenta</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </body>
</html>

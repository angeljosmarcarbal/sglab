<!DOCTYPE html>
<html lang="es-Mx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/logo/logoescuela.png') }}">
    <title>SgLab</title>

    <!-- Styles -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/ruang-admin.min.css') }}" rel="stylesheet">
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

    <script>
      alumno = true; /*1 alumno 0 docente*/
      function isDocente(){
        $('#h-title').text('Docentes')
        $('#label-matricula').text('ID de docente')
        $('#exampleInputEmail').attr('placeholder', 'Tu ID de docente')
        $('#tipo').val('docente')
        $('#btn-user-type').text('Soy alumno')
        $('#btn-dont-account').show()
        alumno = false
      }
      function isAlumno(){
        $('#h-title').text('Alumnos')
        $('#label-matricula').text('Matricula')
        $('#exampleInputEmail').attr('placeholder', 'Tu matricula')
        $('#tipo').val('alumno')
        $('#btn-user-type').text('Soy docente')
        $('#btn-dont-account').hide()
        alumno = true
      }
    </script>

    <style>
      .has-error{
        border: 1px solid red;
      }
    </style>
</head>
<body>

<div class="container-login">
    <div class="row">
      <div class="col-xl-10 col-lg-5 col-md-9 m-auto">
        <div class="card shadow-sm my-4">
          <div class="card-header">
            <center>
              <img src="{{ asset('img/logo/logoescuela.png') }}" width="40%" alt="">
              <h1 class="h4 text-gray-600 mb-4">Iniciar sesión</h1>
              @if($errors->has('saveOk'))
                <div class="alert alert-success" id="alert">{{$errors->first('saveOk')}}</div>
                <script>setTimeout(function(){ $('#alert').hide() }, 6000)</script>
              @endif()
            </center>
          </div>
          <div class="card-body p-0">
            <div class="login-form" style="padding-top: 0!important;">
              <div class="text-center">
                <h4 class="h4 text-primary" id="h-title">Alumnos</h4>
              </div>
              <form class="form-horizontal" id="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}

                <input type="hidden" name="tipo" id="tipo" value="alumno">

                <div class="form-group">
                  <label for="exampleInputEmail" id="label-matricula">Matricula</label>
                  <input type="text" name="matricula" class="form-control {{$errors->has('matricula') ? 'has-error' : ''}}" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Tu matricula" required="" value="{{old('matricula')}}">
                  @if($errors->has('matricula'))
                    <small class="text-danger">{{$errors->first('matricula')}}</small>
                  @endif()
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword">Contraseña</label>
                    <input type="password" class="form-control {{$errors->has('password') ? 'has-error' : ''}}" name="password" id="exampleInputPassword" placeholder="Escribe tu contraseña" required="{{old('password')}}">
                    @if($errors->has('password'))
                    <small class="text-danger">{{$errors->first('password')}}</small>
                  @endif()
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                </div>

                <hr>
              </form>
              <hr>
              <div class="text-center">
                <a class="font-weight-bold small" id="btn-user-type" href="javascript:void(0);">Soy docente</a><br>
                <a class="font-weight-bold small" id="btn-dont-account" href="{{ route('register') }}" style="display: none;">Registrarme (Docente)</a>
              </div>
              <div class="text-center">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <script>

      @if($errors->has('tipo'))
        @if($errors->first('tipo') == 'alumno')
          isAlumno()
        @else()
          isDocente()
        @endif()
      @endif()

      $(function(){
        $('#btn-user-type').click(function(){
          if(alumno){ 
              isDocente()
          }else{ 
              isAlumno()
          }
        })
      })
    </script>
  </body>
</html>

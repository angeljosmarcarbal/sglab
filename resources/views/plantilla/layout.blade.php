<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('plantilla.header')
</head>
<body id="page-top">

	<div id="wrapper">

		@include('plantilla.sidebar')

    	<div id="content-wrapper" class="d-flex flex-column">
      		<div id="content">

      			@include('plantilla.navbar')

		        <!-- Container Fluid-->
		        <div class="container-fluid" id="container-wrapper">

		        	@yield('general-content')

		        </div>
		        <!---Container Fluid-->

      		</div>
	      <!-- Footer -->
	      <footer class="sticky-footer bg-white">
	        <div class="container my-auto">
	          <div class="copyright text-center my-auto">
	            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - desarrollado por
	              <b><a href="https://indrijunanda.gitlab.io/" target="_blank">UTSEM</a></b>
	            </span>
	          </div>
	        </div>
	      </footer>
	      <!-- Footer -->
    </div>
    
  </div>


  @include('plantilla.footer')
  @yield('script')
</body>
</html>
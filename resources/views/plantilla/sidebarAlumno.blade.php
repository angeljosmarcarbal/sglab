<!--Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
          <img src="{{ asset('img/logo/logoescuela.png') }}">
        </div>
        <!-- <div class="sidebar-brand-text mx-3">SgLab</div> -->
      </a>
      <hr class="sidebar-divider my-0">

      <div class="sidebar-heading">
        Bienvenido Alumno
      </div>
      <hr class="sidebar-divider">
      <div class="sidebar-heading">
        Opciones
      </div>

      <li class="nav-item" id="item-perfil">
        <a class="nav-link" href="{{route('index')}}">
          <i class="fas fa-fw fa-user"></i>
          <span>Inicio</span>
        </a>
      </li>

      <li class="nav-item" id="item-perfil">
        <a class="nav-link" href="{{route('perfilAlumno')}}">
          <i class="fas fa-fw fa-user"></i>
          <span>Mi perfil</span>
        </a>
      </li>
      <hr class="sidebar-divider">

      <div class="version" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar-->
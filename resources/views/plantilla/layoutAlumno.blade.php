<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	@include('plantilla.header')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
</head>
<body id="page-top">

<div id="wrapper">

	@yield('sidebar')

	<div id="content-wrapper" class="d-flex flex-column">
		<div id="content">

		@include('plantilla.navbarAlumno')

		<!-- Container Fluid-->
			<div class="container-fluid" id="container-wrapper">

				@yield('general-content')

			</div>
			<!---Container Fluid-->

		</div>
		<!-- Footer -->
		<footer class="sticky-footer bg-white">
			<div class="container my-auto">
				<div class="copyright text-center my-auto">
	            <span>copyright &copy; <script> document.write(new Date().getFullYear()); </script> - desarrollado por
	              <b><a href="https://indrijunanda.gitlab.io/" target="_blank">UTSEM</a></b>
	            </span>
				</div>
			</div>
		</footer>
		<!-- Footer -->
	</div>

</div>


@include('plantilla.footer')
@yield('script')
</body>
</html>
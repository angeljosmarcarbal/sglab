<!--Sidebar -->
    <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon">
          <img src="{{ asset('img/logo/logoescuela.png') }}">
        </div>
        <!-- <div class="sidebar-brand-text mx-3">SgLab</div> -->
      </a>
      <div class="sidebar-heading">
        Opciones
      </div>
      @if(auth()->user()->tieneArea(auth()->user()->rol_id, 1))
      <li class="nav-item" id="admin-general">
        <a class="nav-link collapsed active" href="#" data-toggle="collapse" data-target="#collapseBootstrap"
          aria-expanded="true" aria-controls="collapseBootstrap">
          <i class="far fa-fw fa-window-maximize"></i>
          <span>Administración</span>
        </a>
        <div id="collapseBootstrap" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Administración</h6>
            <a class="collapse-item" href="{{route('roles.show')}}" id="admin-item-roles">Roles</a>
            <a class="collapse-item" href="{{route('usuarios.show')}}" id="admin-item-usuarios">Usuarios</a>
          </div>
        </div>
      </li>
      @endif()
      @if(auth()->user()->tieneArea(auth()->user()->rol_id, 2))
      <li class="nav-item" id="bitacora-general">
        <a class="nav-link collapsed active" href="#" data-toggle="collapse" data-target="#collapseBootstrapBitacora"
          aria-expanded="true" aria-controls="collapseBootstrapBitacora">
          <i class="far fa-fw fa-window-maximize"></i>
          <span>Bitácora</span>
        </a>
        <div id="collapseBootstrapBitacora" class="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Bitácora</h6>
            <a class="collapse-item" href="{{route('bitacora.registros.show')}}" id="bitacora-item-maquinas">Uso de Equipos</a>
            <a class="collapse-item" href="{{route('bitacora.incidencias.show')}}" id="bitacora-item-incidencias">Incidencias</a>
          </div>
        </div>
      </li>
      @endif()
      @if(auth()->user()->tieneArea(auth()->user()->rol_id, 3))
      <li class="nav-item" id="catalogos-general">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseForm" aria-expanded="true"
          aria-controls="collapseForm">
          <i class="fab fa-fw fa-wpforms"></i>
          <span>Catálogos</span>
        </a>
        <div id="collapseForm" class="collapse" aria-labelledby="headingForm" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Catálogos</h6>
            <a class="collapse-item" href="{{route('laboratorios.show')}}" id="catalogos-item-laboratorios">Laboratorios</a>
            <a class="collapse-item" href="{{route('incidentes.show')}}" id="catalogos-item-incidentes">Tipo de incidentes</a>
            <a class="collapse-item" href="{{route('prioridades.show')}}" id="catalogos-item-prioridades">Prioridades</a>
            <a class="collapse-item" href="{{route('status.show')}}" id="catalogos-item-status">Status</a>
          </div>
        </div>
      </li>
      @endif()
      @if(auth()->user()->tieneArea(auth()->user()->rol_id, 4))
      <li class="nav-item" id="reportes-general">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTable" aria-expanded="true"
          aria-controls="collapseTable">
          <i class="fas fa-fw fa-table"></i>
          <span>Reportes</span>
        </a>
        <div id="collapseTable" class="collapse" aria-labelledby="headingTable" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Reportes</h6>
            <a class="collapse-item" href="{{ route('reportes.reporte1') }}" id="reportes-item-reporte-1">Incidencias</a>
            <a class="collapse-item" href="{{ route('reportes.reporte2') }}" id="reportes-item-reporte-2">Incidencias (fecha)</a>
          </div>
        </div>
      </li>
      @endif()
      <li class="nav-item" id="item-perfil">
        <a class="nav-link" href="{{route('perfil')}}">
          <i class="fas fa-fw fa-user"></i>
          <span>Mi perfil</span>
        </a>
      </li>
      <hr class="sidebar-divider">

      <div class="version" id="version-ruangadmin"></div>
    </ul>
    <!-- Sidebar-->
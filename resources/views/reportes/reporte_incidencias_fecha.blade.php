<!-- Reporte de incidencias por máquina -->
@extends('plantilla.layout') 

@section('general-content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Reporte de incidencias por equipo (por fecha).</h1>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Filtros de búsqueda</h6>
				<small>Los campos con * son obligatorios</small>
			</div>
			<div class="card-body">
				<form action="{{route('reportes.data')}}" method="POST" id="form-report-1">
					{{csrf_field()}}
					<div class="row">
					   	<div class="col-lg-3">
					   		<div class="form-group">
					   			<label for="laboratorio">Laboratorio*</label>
					   			<select class="form-control" name="laboratorio" id="laboratorio-select">
					   				<option value="">Elije una opción</option>
					   				@foreach($laboratorios as $laboratorio)
					   				<option value="{{$laboratorio->id}}" {{$laboratorio->status ==1 ? '' : 'disabled'}}>{{$laboratorio->nombre}}</option>
					   				@endforeach()
					   			</select>
					   			@if($errors->has('laboratorio'))
					   				<span class="text-danger">{{$errors->first('laboratorio')}}</span>
					   			@endif()
					   		</div>
					   	</div>
						<div class="col-lg-3">
							<div class="form-group">
					   			<label for="equipo">Equipo*</label>
					   			<select class="form-control" name="equipo" id="equipo-select">
					   				<option value="">Elije una opción</option>
					   			</select>
					   			@if($errors->has('equipo'))
					   				<span class="text-danger">{{$errors->first('equipo')}}</span>
					   			@endif()
					   		</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">
					   			<label for="fecha">Fecha*</label>
					   			<input type="date" class="form-control" id="fecha" name="fecha">
					   			@if($errors->has('fecha'))
					   				<span class="text-danger">{{$errors->first('fecha')}}</span>
					   			@endif()
					   		</div>					   		
						</div>
						<div class="col-lg-3 m-auto">
							<button type="submit" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Buscar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card mb-4">
			<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
				<h6 class="m-0 font-weight-bold text-primary">Resultados de búsqueda</h6>
				<div class="btn-group">
					<!-- @if($errors->has('laboratorioOk') && $errors->has('equipoOk') && $errors->has('fechaOk'))
						<a href="{{ route('reportes.exportarF', [$errors->first('laboratorioOk'), $errors->first('equipoOk'), $errors->first('fechaOk'), 'xlsx']) }}" class="btn btn-success btn-sm"><i class="fa fa-file-excel"></i> Excel</a>
						<a href="{{ route('reportes.exportarF', [$errors->first('laboratorioOk'), $errors->first('equipoOk'), $errors->first('fechaOk'), 'pdf']) }}" class="btn btn-danger btn-sm"><i class="fa fa-file-pdf"></i> PDF</a>
					@endif() -->
				</div>
			</div>
			<div class="card-body">
				<table class="table table-striped table-sm" id="tabla-bitacora-incidencias">
					<thead class="bg-primary text-white" style="font-size: 15px;">
						<th style="width: 10%;">Laboratorio</th>
						<th style="width: 15%;">Maquina</th>
						<th style="width: 20%;">Usuario</th>
						<th style="width: ;">Tipo incidente</th>
						<th style="width: ;">Comentario</th>
						<th style="width: ;">Prioridad</th>
						<th style="width: ;">Status</th>
						<th style="width: ;">Fecha</th>
					</thead>
					<tbody style="font-size: 14px;">
						@foreach($datos as $dato)
							<tr>
								<td>{{$dato->laboratorio}}</td>
								<td>{{$dato->equipo}}</td>
								<td>{{$dato->usuario}}</td>
								<td>{{$dato->incidente}}</td>
								<td>{{$dato->comentario}}</td>
								<td>{{$dato->prioridad}}</td>
								<td>{{$dato->status}}</td>
								<td>{{$dato->fecha}}</td>
							</tr>
						@endforeach()
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script>
	$('#collapseTable').addClass('show');
	$('#reportes-item-reporte-2').addClass('active');
	$(document).ready(function(){
		$('#tabla-bitacora-incidencias').DataTable({
			language:{
				url: "//cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
			},
			dom: 'Bfrtip',
	        buttons: [
	            {
	            	extend: 'excelHtml5',
	            	text: '<i class="fa fa-file-excel"></i> Excel',
	            	titleAttr: 'Exportar a excel',
	            	className: 'btn btn-success btn-sm'
	            },
	            {
	            	extend: 'pdfHtml5',
	            	text: '<i class="fa fa-file-pdf"></i> PDF',
	            	titleAttr: 'Exportar a pdf',
	            	className: 'btn btn-danger btn-sm'
	            }
	        ]
		});

		$('#laboratorio-select').val("{{$errors->first('laboratorioOk')}}");
		getEquipos("{{$errors->first('laboratorioOk')}}");
		$('#fecha').val("{{$errors->first('fechaOk')}}");

		$('#laboratorio-select').change(function(){
			getEquipos($(this).val());
		});

		function getEquipos(laboratorio){
			$.post("{{route('reportes.em')}}",{
				'_token': '{{csrf_token()}}',
				'laboratorio': laboratorio
			}).done(function(response){
				//console.log(data);
				let select = $('#equipo-select');
	          	select.empty();    
	          	select.append($('<option>',{
	          		value: '',
	          		text: 'Elije una opción'
	          	}));        
	          	$.each(response, function (i, item) {
	            	select.append($('<option>', { 
	                	value: item.id,
	                	text : item.numero+' '+item.marca+' '+item.modelo
	            	}));
	          	});
	          	select.val("{{$errors->first('equipoOk')}}");
			});
		}
	})
</script>
@endsection()
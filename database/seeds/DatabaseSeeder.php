<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('areas')->insert([
        	[
		        'codigo'  => 1,
		        'nombre'  => 'Administración',
		        'status'  => 1,
		        'created_at' => date('Y-m-d'),
		        'updated_at' => date('Y-m-d')
		    ],[
		    	'codigo'  => 2,
		        'nombre'  => 'Bitácora',
		        'status'  => 1,
		        'created_at' => date('Y-m-d'),
		        'updated_at' => date('Y-m-d')
		    ],[
		    	'codigo'  => 3,
		        'nombre'  => 'Catálogo',
		        'status'  => 1,
		        'created_at' => date('Y-m-d'),
		        'updated_at' => date('Y-m-d')
		    ],[
		    	'codigo'  => 4,
		        'nombre'  => 'Reportes',
		        'status'  => 1,
		        'created_at' => date('Y-m-d'),
		        'updated_at' => date('Y-m-d')
		    ]
            ,[
                'codigo'  => 5,
                'nombre'  => 'Alumno',
                'status'  => 1,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]
		]);

		DB::table('roles')->insert([
	        'nombre'  => 'Súper Administrador',
	        'areas' => '1,2,3,4',
	        'eliminable' => 0,
	        'status'  => 1,
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);

        DB::table('roles')->insert([
            'nombre'  => 'Alumno / Docente',
            'areas' => '5',
            'eliminable' => 0,
            'status'  => 1,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        DB::table('prioridades')->insert([
        	'tipo' => 1,
            'nombre'  => 'Alta',
            'descripcion' => 'Alta',
            'status'  => 1,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

		DB::table('users')->insert([     
	        'nombre'  => 'Angel',
	        'apellido_paterno'  => 'Josmar',
	        'apellido_materno' => 'Carbajal',
	        'email' => 'angeljosmarcarbal@gmail.com',
	        'matricula' => '20173TI000',
	        'password' => bcrypt('12345'),
	        'telefono' => '7770001122',
	        'rol_id' => 1,
	        'status'  => 1,
	        'remember_token' => '',
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);

        DB::table('status')->insert([
            'nombre'  => 'Recibido',
            'atendido_por'  => 'Recibido',
            'descripcion'  => 'Detalla tu problema',
            'status' => '1'
        ]);

        DB::table('tipos_de_incidentes')->insert([
            'tipo'  => '1',
            'nombre'  => 'Falta de software',
            'descripcion'  => 'Detalla tu problema',
            'status' => '1'
        ]);

		DB::table('laboratorios')->insert([     	      
	        'nombre'  => 'CC1',
	        'status' => '1',       
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);
		DB::table('laboratorios')->insert([     	      
	        'nombre'  => 'CC2',
	        'status' => '1',       
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);
		DB::table('laboratorios')->insert([     	      
	        'nombre'  => 'MAC',
	        'status' => '1',       
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);
        for ($i=1; $i < 30; $i++) {
            DB::table('computadoras')->insert([
                'laboratorio_id'  => '1',
                'numero' => $i,
                'marca' => 'Hp',
                'modelo' => '3250',
                'status' => '1',
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ]);
        }

		for ($i=1; $i < 25; $i++) { 
			DB::table('computadoras')->insert([     	      
	        'laboratorio_id'  => '2',
	        'numero' => $i,
	        'marca' => 'Dell',
	        'modelo' => 'Vostro',
	        'status' => '1',	       
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);
		}

		for ($i=1; $i < 26; $i++) { 
			DB::table('computadoras')->insert([     	      
	        'laboratorio_id'  => '3',
	        'numero' => $i,
	        'marca' => 'Apple',
	        'modelo' => 'Mac Pro',
	        'status' => '1',	       
	        'created_at' => date('Y-m-d'),
	        'updated_at' => date('Y-m-d')
		]);
		}

    }

}

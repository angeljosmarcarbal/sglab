<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComputadorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('computadoras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('laboratorio_id')->unsigned();
            $table->integer('numero');
            $table->string('marca');
            $table->string('modelo');
            $table->integer('status')->comment('Status 1: Status activo; Status 0: Status inactivo');
            $table->timestamps();
            $table->foreign('laboratorio_id')->references('id')->on('laboratorios')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('computadoras');
    }
}

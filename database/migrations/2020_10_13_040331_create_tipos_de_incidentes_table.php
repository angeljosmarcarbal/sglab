<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposDeIncidentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_de_incidentes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo');
            $table->string('nombre', 150);
            $table->string('descripcion', 255);
            $table->integer('status')->comment('Status 1: activo, Status 0: inactivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_de_incidentes');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Usoincidentes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uso_incidentes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_uso');
            $table->integer('id_TipoIncidente');
            $table->string('detalles', 255);
            $table->integer('id_prioridad')->unsigned();
            $table->integer('status')->comment('Status 2: resuelto, Status 1: revisado, Status 0: recibido');
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

$(function(){
   //alert('script agregado');
    $('#laboratorio').on('change', onSelectLaboratorioChange);
});

function onSelectLaboratorioChange(){
	
    var laboratorio_id= $(this).val();
    console.log("entro "+laboratorio_id);
    //AJAX
	if(!laboratorio_id)
		$('#numero').html('<option value="">Seleccione un numero de maquina</option>');

    $.get('/api/laboratorio/'+laboratorio_id+'/equipos', function(data){
    	var html_select = '<option value="">Seleccione un numero de maquina</option>';
    	for(var i=0; i<data.length; i++){
    		html_select = html_select+'<option value="'+data[i].id+'">'+data[i].numero+' '+data[i].marca+'</option>';

    	}
    	console.log(html_select);
    	$('#numero').html(html_select);
    });
}